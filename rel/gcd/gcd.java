import java.utils.*;

public class Main {
  public static void main (String[] p) {
    Interface goInterface = new Interface ();
    int vA = goInterface.varGcd ();
    int vB = goInterface.varGcd ();
    objGcd solvedGcd = new objGcd (vA, vB);
    goInterface.mR ("\n"+objGcd.cGnd+"\n");
  }
}

public class objGcd () {
  private a;
  private b;

  public objGcd (int pA, int pB) {
    if (pA < 0 && pB < 0) {
      a=a*-1;
      b=b*-1;
    } else if (pA < 0) {
      a = a*-1;
    } else if (pB < 0) {
      b = b*-1;
    }
  }

  public int cGnd () {
    if (b == 0) {
      return a;
    }
    return cGnd (b, a%b);
  }
}

class Interface () {
  public Interface () {}
  public void mR (String str) {
    System.out.print ("\nGreatest Common Divisor >>\n"str"\n>:)\n");
  }
  public void varGcd () {
    System.out.print ("Digite valores para a e b\n");
    Scanner leitor = new Scanner (System.in);
    int a = leitor.nextInt ();
    int b = leitor.nextInt ();
    return new objGcd (a, b);
  }
}
