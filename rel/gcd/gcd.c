#include <stdio.h>
#include <stdlib.h>

typedef int bool
#define false 0
#define true 1

int gcd (int a, int b) {
  while (a!=0) {
   c = a;
   a=b%a;
   b=c;
  }
  return b;
}

int gcdr (int a, int b) {
  if (a == 0) {
    return b;
  }
  return gcdr (b%a, a);
}
/*Extended GCD (Euclidean Algorithm)
Given a,b, Find x,y,g that solve the equation: ax+by=g=gcd(a,b)*/
void egcd (int a, int b) {
  int x=0,i=1,u=1,v=0,m=0,n=0,q=0,r=0,gcd=b;
  while (a!=0) {
    q=gcd/a;
    r=gcd%a;
    m=x-u*q;
    n=y-v*q;
    gcd=a;
    a=r;
    x=u;
    y=v;
    u=m;
    v=n;
  }
}
void egcdr (int a, int b) {
  if (b==0) {
    return (a,1,0); //ax=by=a
  }
  
}
int main (int args[], char *args[]) {
  int a = 0, b = 0, c = 0;
  bool t = true;
  printf ("Algo para calcular MCD (en: GCD)\nDigite valor para a:");
  scanf ("%i",&a);
  printf ("\nDigite valor para b: ");
  scanf ("%i",&b);
  printf ("\nDigite valor para c: ");
  scanf ("%i",&c);

  while (t == true) {
    if (a > 0 && b > 0 && c > 0) {
      printf ("\nMDC >> \n%i\nterminado.\n",gcd (a, b));
      printf ("\nMDC >> \n%i\nterminado.\n",gcdr (a, gcd (b,c)));
      t = false;
    } else if (a < 0) {
      printf ("\na menor que zero, invalido\n");
    } else if (b < 0) {
      printf ("\nb menor que zero, invalido\n");
    } else if (c < 0) {
      printf ("\nc menor que zero, invalido\n");
    }
  }
}
