#!/usr/bin/env python

def gcd(a, b):
    while a:
        a, b = b%a, a
    return b

a=137
b=13

print gcd(a, b)
