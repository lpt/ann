#!/usr/bin/env python

def warshall(graph):
    index = range(len(graph))
    for k in index:
        for i in index:
            for j in index:
                graph[i][j] = graph[i][j] or graph[i][k] and graph[k][j]
#                   print graph
#                   print "\n"
    return graph

a = [[0, 1, 0, 0], [1, 0, 1, 0], [0, 0, 0, 1], [0, 0, 0, 0]]
print warshall(a)
a = [[0, 1, 0], [0, 0, 1], [0, 0, 0]]
print warshall(a)

#(1) (2) (3) observem que no estado i=0, j=3 e k=2 o algoritimo
#verifica que na posicao g(1,3) == 0 mas as posicoes g(2,3) e
#g(1,2) == 1. Indicando um caminho direto de 1 para 3.
