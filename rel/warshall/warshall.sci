function [n,p]=warshall(graph)
    index = range (len(graph))
    for k := 1 to index
        for i := 1 to index
            for j := 1 to index
                graph(i)(j) = graph(i)(j) or graph(i)(k) and graph(k)(j)
                print graph
                print "\n"
            end
        end
    end
    return graph
endfunction

a = [0 1 0 0; 1 0 1 0; 0 0 0 1; 0 0 0 0]
x = warshall (a)
a = [0 1 0; 0 0 1; 0 0 0]
x = warshall (a)
