function z=zero_sec(f,x0,x1,Nmax); // Método da Secante

//dados de entrada

// x0       -> extremidade inferior.
// x1       -> extremidade superior.
// Nmax     -> limite superior para o número de iterações.

// variáveis auxiliares

// f0           -> valor da função f em x0.
// f1           -> valor da função f em x1.
// abs_diff     -> diferença absoluta entre duas aproximações.
// old_abs_diff -> diferença absoluta na iteração anterior.
// contador     -> guarda o número de iteradas realizadas.
// segue        -> variável booleana. Controla o fluxo de execução das iteradas.
// convergiu    -> variável booleana. Guarda informação sobre a convergência.

//inicialiazação das variáveis

f0=f(x0);
f1=f(x1);

if x0<>x1 & f0==f1 then
  warning('Verifique o condicionamento do problema\n');
  segue=%F;
else
  x2=x1-((x1-x0)/(f1-f0))*f1;
end;

if x2==x1 | x2==x0 then 
  segue=%F;
else 
  segue=%T; 
end;

convergiu=%F;
old_abs_diff=abs(x2-x1);

x0=x1;
f0=f1;

x1=x2;
f1=f(x1);

contador=1;

//laço principal

while segue
  contador=contador+1;
  
  if abs(f1)>abs(f0) then 
    x2=x1-((x1-x0)/(1-f0/f1));
  else
    x2=x1-(x0-x1)/(1-f1/f0)*(f1/f0);
  end;
  
  abs_diff=abs(x2-x1);
  x0=x1;
  f0=f1;
  
  x1=x2;
  f1=f(x1);
  
  // descomente a linha abaixo se quiseres a sequência de iterações no console
  //mprintf('Iteração %d \t aproximação: %.16e\n',contador,x1);
  
  if abs_diff>=old_abs_diff then 
    convergiu=%T;
  end;
  
  if convergiu | f1==0 | f0/f1==1 | f1/f0==1 | contador>=Nmax then
     segue=%F;   // teste para o prosseguimento do laço principal.
  end;
end; 

// Saída de dados

// mensagens de aviso
if contador>=Nmax then 
  warning(msprintf('\nA exatidão não foi obtida em %d iteracoes\n',contador));
  if f1==0 | f0==f1 then
    warning(msprintf('Verifique o condicionamento do problema.'));
  end;
else,
  warning(msprintf('Foram necessárias %d iterações \n\n',contador)),
end;

z=x1;

endfunction
