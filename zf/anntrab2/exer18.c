#include <stdio.h>
#include <math.h>
/*
f(x)=sen(PIx)
Zeros: x=n onde nEZ
-1 < a < 0 --o--a--o------------
            -1     0       
                           
 2 < b < 3 ------------o--b--o--
                       2     3

           --o~~a~~o-+-o~~b~~o--
            -1     0   2     3
a. Converge para 0 se a + b < 2
b. Converge para 2 se a + b > 2
c. Converge para 1 se a + b = 2
Exercício 18 letra a, b e c.
*/
#define PI 3.1415926535897
main()
{
   double f_a,f_mid,a,b,m;
   int i=0;
   a=-0.5;
   b=2.5;
   while ((b-a)>10e-9)
   {
      i++;
      m=(a+(b-a)/2);
      f_mid=((sin((PI*m))));
      f_a=((sin((PI*a))));
      if ((f_a*f_mid)>0)
      {
         a=m;
      }
      else
      {
         b=m;
      }
      printf ("iterac: %i\nf(a): %.8f\nf(mid): %.8f\na: %.8f\nb: %.8f\nm: %.8f\n--------------------\n",i,f_a,f_mid,a,b,m);
   }
};
/*
GCC OUTPUT
Não mostrei todas as iterações, por questão de espaço, mas já é possível visualizar o resultado convergindo, partindo da 21ª iteração.

Intervalo [-0.1;2] x=n onde nEZ
Por exemplo -0.1+2 = 1.9 e 1.9 é menor que 2, Então para valores de 'a' mais valores de 'b' que resultam em um valor menor que 2 temos um resultando convergindo para 0, como consta na letra a.

iterac: 21
f(a): -0.00000120
f(mid): 0.00000195
a: -0.00000038
b: 0.00000062
m: 0.00000062
--------------------
iterac: 22
f(a): -0.00000120
f(mid): 0.00000037
a: -0.00000038
b: 0.00000012
m: 0.00000012
--------------------
iterac: 23
f(a): -0.00000120
f(mid): -0.00000041
a: -0.00000013
b: 0.00000012
m: -0.00000013
--------------------
iterac: 24
f(a): -0.00000041
f(mid): -0.00000002
a: -0.00000001
b: 0.00000012
m: -0.00000001
--------------------
iterac: 25
f(a): -0.00000002
f(mid): 0.00000018
a: -0.00000001
b: 0.00000006
m: 0.00000006
--------------------
iterac: 26
f(a): -0.00000002
f(mid): 0.00000008
a: -0.00000001
b: 0.00000003
m: 0.00000003
--------------------
iterac: 27
f(a): -0.00000002
f(mid): 0.00000003
a: -0.00000001
b: 0.00000001
m: 0.00000001
--------------------
iterac: 28
f(a): -0.00000002
f(mid): 0.00000001
a: -0.00000001
b: 0.00000000
m: 0.00000000
--------------------

Intervalo [-0.1;2.5] x=n onde nEZ
Por exemplo -0.1+2.5 = 2.4 e 2.4 é maior que 2, Então para valores de a mais valores de b que resultam em um valor maior que 2 temos um resultando convergindo para 2, como consta na letra b.

iterac: 21
f(a): -0.00000599
f(mid): -0.00000210
a: 1.99999933
b: 2.00000057
m: 1.99999933
--------------------
iterac: 22
f(a): -0.00000210
f(mid): -0.00000015
a: 1.99999995
b: 2.00000057
m: 1.99999995
--------------------
iterac: 23
f(a): -0.00000015
f(mid): 0.00000082
a: 1.99999995
b: 2.00000026
m: 2.00000026
--------------------
iterac: 24
f(a): -0.00000015
f(mid): 0.00000034
a: 1.99999995
b: 2.00000011
m: 2.00000011
--------------------
iterac: 25
f(a): -0.00000015
f(mid): 0.00000009
a: 1.99999995
b: 2.00000003
m: 2.00000003
--------------------
iterac: 26
f(a): -0.00000015
f(mid): -0.00000003
a: 1.99999999
b: 2.00000003
m: 1.99999999
--------------------
iterac: 27
f(a): -0.00000003
f(mid): 0.00000003
a: 1.99999999
b: 2.00000001
m: 2.00000001
--------------------
iterac: 28
f(a): -0.00000003
f(mid): 0.00000000
a: 1.99999999
b: 2.00000000
m: 2.00000000
--------------------

Intervalo [-0.5;2.5] x=n onde nEZ
Bom, colocando valores que estão dentro do padrão dos intervalos do exercício 18, Por exemplo -0.3+2.3 eu obtenho resultado igual a 2, no entando, pelo método da bisseção, vemos que converge para 0, e não 1 como consta na letra c.

iterac: 21
f(a): -0.00000599
f(mid): -0.00000150
a: -0.00000048
b: 0.00000095
m: -0.00000048
--------------------
iterac: 22
f(a): -0.00000150
f(mid): 0.00000075
a: -0.00000048
b: 0.00000024
m: 0.00000024
--------------------
iterac: 23
f(a): -0.00000150
f(mid): -0.00000037
a: -0.00000012
b: 0.00000024
m: -0.00000012
--------------------
iterac: 24
f(a): -0.00000037
f(mid): 0.00000019
a: -0.00000012
b: 0.00000006
m: 0.00000006
--------------------
iterac: 25
f(a): -0.00000037
f(mid): -0.00000009
a: -0.00000003
b: 0.00000006
m: -0.00000003
--------------------
iterac: 26
f(a): -0.00000009
f(mid): 0.00000005
a: -0.00000003
b: 0.00000001
m: 0.00000001
--------------------
iterac: 27
f(a): -0.00000009
f(mid): -0.00000002
a: -0.00000001
b: 0.00000001
m: -0.00000001
--------------------
iterac: 28
f(a): -0.00000002
f(mid): 0.00000001
a: -0.00000001
b: 0.00000000
m: 0.00000000
--------------------
iterac: 29
f(a): -0.00000002
f(mid): -0.00000001
a: -0.00000000
b: 0.00000000
m: -0.00000000
--------------------
*/
