#include <stdio.h>
#include <math.h>
/*
f(x)=omega+2sinomega-2sinh(omega)-(32.17/6.8)
Exercício 20
Notas: Segundo as relações trigonométricas eu posso considerar -e^omega+e^-omega = 2sinh. Que são as relações trigonométricas de sinh. Fiz isso, pois ficou muito mais fácil, derivar a função dessa forma.
*/
main()
{
   int i=0;
   double omega=-2,dx=1,funcao,f_derivada,xlinha,erro;
   while (((dx>10e-9) || (fabs(((0.21137)*(pow(omega,2)))+(2*(sinh(omega)))-(2*(sin(omega)))))>10e-9))
   {
      i++;
      f_derivada=(((0.42274)*omega)+(2*(cosh(omega)))-(2*(cos(omega))));
      funcao=(((0.21137)*(pow(omega,2)))+(2*(sinh(omega)))-(2*(sin(omega))));
      erro=(funcao/f_derivada);
      xlinha=(omega-erro);
      omega=xlinha;
      dx=(fabs(omega-xlinha));
      printf("iteracoes:%i\nx:%.8f\ndx:%.8f\nfuncao:%.8f\nf_derivada:%.8f\nxlinha:%.8f\nerro:%.8f\n---------------------\n",i,omega,dx,funcao,f_derivada,xlinha,erro);
   }
};
/*
GCC OUTPUT
Intervalo [-1,1] omega'=-0.31705121 (aprox.)
Newton-Raphson é realmente mais eficaz para achar raiz de uma função, em algorítimo, além de menos iterações, ele congerge para um resultado mais convincente.

iteracoes:1
x:-1.38896010
dx:0.00000000
funcao:-4.58964596
f_derivada:7.51120506
xlinha:-1.38896010
erro:-0.61103990
---------------------
iteracoes:2
x:-0.97021429
dx:0.00000000
funcao:-1.38653900
f_derivada:3.31117100
xlinha:-0.97021429
erro:-0.41874581
---------------------
iteracoes:3
x:-0.69228897
dx:0.00000000
funcao:-0.41052841
f_derivada:1.47711772
xlinha:-0.69228897
erro:-0.27792532
---------------------
iteracoes:4
x:-0.51231209
dx:0.00000000
funcao:-0.11995125
f_derivada:0.66648140
xlinha:-0.51231209
erro:-0.17997688
---------------------
iteracoes:5
x:-0.40152498
dx:0.00000000
funcao:-0.03417261
f_derivada:0.30845298
xlinha:-0.40152498
erro:-0.11078710
---------------------
iteracoes:6
x:-0.34207063
dx:0.00000000
funcao:-0.00908030
f_derivada:0.15272723
xlinha:-0.34207063
erro:-0.05945436
---------------------
iteracoes:7
x:-0.32024424
dx:0.00000000
funcao:-0.00195186
f_derivada:0.08942659
xlinha:-0.32024424
erro:-0.02182638
---------------------
iteracoes:8
x:-0.31711362
dx:0.00000000
funcao:-0.00021833
f_derivada:0.06973869
xlinha:-0.31711362
erro:-0.00313062
---------------------
iteracoes:9
x:-0.31705121
dx:0.00000000
funcao:-0.00000419
f_derivada:0.06707113
xlinha:-0.31705121
erro:-0.00006241
---------------------


*/
