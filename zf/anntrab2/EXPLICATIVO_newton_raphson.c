#include <stdio.h>
#include <math.h>
/*
Utilizei um função qualquer. para o exemplo explicativo do método de Newton-Raphson
*/
main()
{
   int i=0; //Não influência na parte restante do código, i apenas conta o número de iterações preciso para convergir até a raiz.
   double x=-2,dx=1,funcao,f_derivada,xlinha,erro; //Variáveis declaradas em "double", poderia ser em long double.
   while ((dx>10e-9) || ((fabs((pow(x,3))+3*x+1))>10e-9)) //dx > tolerância E f(-2)=-13 > tolerância, faça o numéro de iterações possíveis!
   {
      i++; //i=0+1,i=1+1,i=2+1,i=3+1,...,sucessivamente. 
      f_derivada=((3*pow(x,2))+3); //Derivada da função
      funcao=((pow(x,3))+(3*x)+1); //Função
      erro=(funcao/f_derivada); //Erro, igualmente no método de Newton-Raphson, trabalha-se com E ~~ F(X)/F'(x)
      xlinha=(x-erro); //Acrescenta o erro a x
      x=xlinha; //Definitivamente acrescenta o erro a x, poderia excluir essa parte, e fazer direto x=(x-erro), mas prefiri assim, para visualizar melhor.
      dx=(fabs(x-xlinha)); //dx recebe o desta operação |x-xlinha|, é desta maneira que dx vai de 1 até 0.00000001
      printf("iteracoes:%i\nx:%.8f\ndx:%.8f\nfuncao:%.8f\nf_derivada:%.8f\nxlinha:%.8f\nerro:%.8f\n---------------------\n",i,x,dx,funcao,f_derivada,xlinha,erro); //printf (argumentos e parâmetros) mostra na tela todos os resultados, e todas as variáveis. inclusive a raiz que fora achada.
   }
};

/*
GCC OUTPUT
Raiz achada x'= -0.32218535 (aprox.)

iteracoes:1
x:-1.13333333
dx:0.00000000
funcao:-13.00000000
f_derivada:15.00000000
xlinha:-1.13333333
erro:-0.86666667
---------------------
iteracoes:2
x:-0.57073065
dx:0.00000000
funcao:-3.85570370
f_derivada:6.85333333
xlinha:-0.57073065
erro:-0.56260268
---------------------
iteracoes:3
x:-0.34491904
dx:0.00000000
funcao:-0.89809804
f_derivada:3.97720043
xlinha:-0.34491904
erro:-0.22581161
---------------------
iteracoes:4
x:-0.32234116
dx:0.00000000
funcao:-0.07579185
f_derivada:3.35690744
xlinha:-0.32234116
erro:-0.02257788
---------------------
iteracoes:5
x:-0.32218536
dx:0.00000000
funcao:-0.00051597
f_derivada:3.31171148
xlinha:-0.32218536
erro:-0.00015580
---------------------
iteracoes:6
x:-0.32218535
dx:0.00000000
funcao:-0.00000002
f_derivada:3.31141022
xlinha:-0.32218535
erro:-0.00000001
---------------------


*/
