/*
Já aviso de ante mão, que para calcular outros polinômios quaisquer, terá de ser alterado no código isso, eu não criei uma interface que pede para o usuário. Pois esse programa tem outros fins, fins de análise numérica. Outra coisa, double pode ser alterado para long double (maior precisão), ao passo é preciso alterar também a função pow para powl ("pow long"), assim como no printf é preciso alterar o tipo para %Le. Então o programa fluirá normalmente.
*/
#include <stdio.h>
#include <math.h>
main()
{
   double f_a,f_mid,a,b,m; // f_a "função de a" f_b (função de b) f_mid (função média)
   int i=0;               // para contagem começaremos com i=0
   a=-1;                 // por exemplo escolho, pontos aleatórios na reta cartesiana x (-1, poderia ser ...,-2,-1,0,1,2,3,4...
                        // ou ainda números fracionádos)
   b=6;                // outro ponto, entendamos que b>a //imagine um plano cartesiano

/*


                  y
                  |
               *  |            *
               {--|---------<--}
    -----------1--|---root--m--6---x
               a  |            b
               *  |            *
                  |



*/

   while ((b-a)>(1*10e-2))  // 1*10e-2 é uma notação matemática para 0.01, que por sua vez é a tolerância quanto
                           // menor a tolerância, maior a precisão da raíz.
                          // nesse caso enquanto b-a for menor que a tolerância, quer dizer que enquanto
                         // b-a não passa da tolerância continue tentando.... (simples)
   {
      i++;                // contagem normal de 0 até onde a tolerãncia permitir, só que esse i++ não influência em nada, ele só mostra no final
                         // quantas vezes foi preciso para achar a raiz
      m=(a+((b-a)/2));  // aqui eu calculo: (a + média entre 'a' e 'b'), ou seja, eu preciso que o valor "ande" sobre a reta x, "procurando" a nossa raiz.
                       // nesse caso ele vai estár sempre um pouco a direita da raiz, na reta x, poderia ser as contrário, mas eu preferi que ele fosse
                                    // "procurando a raiz da direita para a esquerda."
      f_mid=((pow(m,2))-6);        // então a cada cálculo da média entre a e b, mais a. Eu preciso calcular o resultado disso na 
                                  // função principal, que no meu caso é x²-6
      f_a=((pow(a,2))-6);        // e preciso calcular a mesma funcao com o valor de a, pois eu preciso fazer essa verificação, 
                                // pois senão eu ficaria procurando
                               // infinitamente e nunca acharia raiz nenhuma..
      if ((f_a*f_mid)>0)      // finalmente eu comparo o sinal das duas funções, f_a * f_mid > 0 (sinal positivo) do contrário sinal (negativo)
      {
         a=m;                   // resultando num sinal positivo entre a comparação das funções no if anterior, a recebe o valor de m
                               // (m era o novo ponto na reta x), desse modo eu "desloco (b-a)", então eu preciso alterar o valor da 
                              // função da f_a=((pow(a,2))-6);
      }                      // e preciso também que ele verifique ((b-a)>(1*10e-2)) novamente, para posteriormente verificar
                            // dinovo os sinais de f_a e f_mid.
      else
      {
         b=m;            // caso der negativo na comparação, b recebe o valor de m (m era o novo ponto na reta x).
      }
   }
   printf ("iterac: %i\n",i);                                                        // aqui ele mostra número de contagens
   printf ("f(a):\t%f\nf(mid):\t%f\na:\t%f\nb:\t%f\nm:\t%f\n",f_a,f_b,f_mid,a,b,m); // essa coisa, mostra os resultados finals,
                                                                                   // de cada variável no programa
                                                                                  // onde (m) contem o resultado da raiz, natural, pois m é o
                                                                                 // ponto onde quase cai em cima da raiz da função.
};
/*
Para compilar e rodar o programa no gcc (GNU C COMPILER), basicamente segue os comandos a baixo.
>> gcc -lm bisecmethod.c -o bisecmethod
>> ./bisecmethod

GCC OUTPUT

Usando Tolerância 10e-2 ((b-a)>(1*10e-2))

iterac: 7  (NÚMERO DE ITERAÇÕES)
f(a):      -0.284912
f(mid):    -0.020447
a:          2.445312
b:          2.500000
m:          2.445312 (RAIZ)

Usando Tolerância 10e-5 ((b-a)>(1*10e-5))

iterac: 17 (NÚMERO DE ITERAÇÕES)
f(a):      -0.000057
f(mid):     0.000205
a:          2.449478
b:          2.449532
m:          2.449532 (RAIZ)
*/
