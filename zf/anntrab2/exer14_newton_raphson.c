#include <stdio.h>
#include <math.h>
//f(x)=x^3+x-4
//Exercício 14
main()
{
   int i=0;
   double x=-1,dx=1,funcao,f_derivada,xlinha,erro;
   while ((dx>10e-9) || ((fabs((pow(x,3))+(x)-4))>10e-9))
   {
      i++;
      f_derivada=((3*pow(x,2))+1);
      funcao=(((pow(x,3))+(x)-4));
      erro=(funcao/f_derivada);
      xlinha=(x-erro);
      x=xlinha;
      dx=(fabs(x-xlinha));
      printf("iteracoes:%i\nx:%.16f\ndx:%.16f\nfuncao:%.16f\nf_derivada:%.16f\nxlinha:%.16f\nerro:%.16f\n---------------------\n",i,x,dx,funcao,f_derivada,xlinha,erro);
   }
};
/*
GCC OUTPUT
Todas as iterações começando da primeira, é claro que menos iterações aparecem, pois o método Newton-Raphson é muito mais eficaz que o método da biseção. é um ótimo método.

Intervalo [1,4] x'= 1.3787967001383330 (aprox.)

iteracoes:1
x:0.5000000000000000
dx:0.0000000000000000
funcao:-6.0000000000000000
f_derivada:4.0000000000000000
xlinha:0.5000000000000000
erro:-1.5000000000000000
---------------------
iteracoes:2
x:2.4285714285714288
dx:0.0000000000000000
funcao:-3.3750000000000000
f_derivada:1.7500000000000000
xlinha:2.4285714285714288
erro:-1.9285714285714286
---------------------
iteracoes:3
x:1.7464129756706177
dx:0.0000000000000000
funcao:12.7521865889212869
f_derivada:18.6938775510204103
xlinha:1.7464129756706177
erro:0.6821584529008111
---------------------
iteracoes:4
x:1.4436605042470478
dx:0.0000000000000000
funcao:3.0728996938946782
f_derivada:10.1498748447721052
xlinha:1.4436605042470478
erro:0.3027524714235699
---------------------
iteracoes:5
x:1.3812715673669120
dx:0.0000000000000000
funcao:0.4524737030538466
f_derivada:7.2524669545685221
xlinha:1.3812715673669120
erro:0.0623889368801359
---------------------
iteracoes:6
x:1.3788004726725263
dx:0.0000000000000000
funcao:0.0166149820015047
f_derivada:6.7237334284487371
xlinha:1.3788004726725263
erro:0.0024710946943859
---------------------
iteracoes:7
x:1.3787967001383330
dx:0.0000000000000000
funcao:0.0000252883236955
f_derivada:6.7032722303259460
xlinha:1.3787967001383330
erro:0.0000037725341932
---------------------
*/
