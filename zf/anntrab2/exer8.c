#include <stdio.h>
#include <math.h>
//f(x) = tan(x)=x -> tan(x)-x = 0
//Primeira raiz positiva
//Exercício 8 letra b
main()
{
   double f_a,f_mid,a,b,m;
   int i=0;
   a=2;
   b=5;
   while ((b-a)>10e-9)
   {
      i++;
      m=(a+((b-a)/2));
      f_mid=((tan(m))-(m));
      f_a=((tan(a))-(a));
      if ((f_a*f_mid)>0)
      {
         a=m;
      }
      else
      {
         b=m;
      }
      printf ("iterac: %i\nf(a): %.8f\nf(mid): %.8f\na: %.8f\nb: %.8f\nm: %.8f\n--------------------\n",i,f_a,f_mid,a,b,m);
   }
};
/*
GCC OUTPUT
Como o gráfico é simétrico para os quadrantes 1º e 3º do plano cartesiano. Achando a primeira raiz positiva eu tenho uma raiz igual numericamente porém de sinal oposto a ela, localizada no 3º quadrante.

Intervalo [3,5] x'=+4.49340945

iterac: 21
f(a): -0.00004459
f(mid): -0.00001571
a: 4.49340868
b: 4.49341011
m: 4.49340868
--------------------
iterac: 22
f(a): -0.00001571
f(mid): -0.00000127
a: 4.49340940
b: 4.49341011
m: 4.49340940
--------------------
iterac: 23
f(a): -0.00000127
f(mid): 0.00000595
a: 4.49340940
b: 4.49340975
m: 4.49340975
--------------------
iterac: 24
f(a): -0.00000127
f(mid): 0.00000234
a: 4.49340940
b: 4.49340957
m: 4.49340957
--------------------
iterac: 25
f(a): -0.00000127
f(mid): 0.00000054
a: 4.49340940
b: 4.49340948
m: 4.49340948
--------------------
iterac: 26
f(a): -0.00000127
f(mid): -0.00000036
a: 4.49340944
b: 4.49340948
m: 4.49340944
--------------------
iterac: 27
f(a): -0.00000036
f(mid): 0.00000009
a: 4.49340944
b: 4.49340946
m: 4.49340946
--------------------
iterac: 28
f(a): -0.00000036
f(mid): -0.00000014
a: 4.49340945
b: 4.49340946
m: 4.49340945
--------------------
iterac: 29
f(a): -0.00000014
f(mid): -0.00000002
a: 4.49340946
b: 4.49340946
m: 4.49340946
--------------------

*/
