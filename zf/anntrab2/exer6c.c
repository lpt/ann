#include <stdio.h>
#include <math.h>
//f(x)= x² - 4x - lnx + 4
//Exercício 6 letra c
//log(x)=ln[x] na matemática 
main()
{
   double f_a,f_mid,a,b,m;
   int i=0;
   a=2;
   b=4;
   while ((b-a)>10e-9)
   {
      i++;
      m=(a+((b-a)/2));
      f_mid=((m*m)-(4*m)-(log(m))+4);
      f_a=((a*a)-(4*a)-(log(a))+4);
      if ((f_a*f_mid)>0)
      {
         a=m;
      }
      else
      {
         b=m;
      }
      printf ("iterac: %i\nf(a): %.8f\nf(mid): %.8f\na: %.8f\nb: %.8f\nm: %.8f\n--------------------\n",i,f_a,f_mid,a,b,m);
   }
};
/*
GCC OUTPUT
Não mostrei todos as iterações, por motivos de espaço, no entanto, ainda podemos visualizar como o programa trabalhou, partindo da 18º em diante. E apesar de o exercício 6 pedir apenas as raízes positivas em todas as letras, mostrei todas as raízes possíveis.


Intervalo [1,2] x'= 1.41239117 (aprox.)

iterac: 18
f(a): 0.00001344
f(mid): 0.00000626
a: 1.41238785
b: 1.41239166
m: 1.41238785
--------------------
iterac: 19
f(a): 0.00000626
f(mid): 0.00000267
a: 1.41238976
b: 1.41239166
m: 1.41238976
--------------------
iterac: 20
f(a): 0.00000267
f(mid): 0.00000087
a: 1.41239071
b: 1.41239166
m: 1.41239071
--------------------
iterac: 21
f(a): 0.00000087
f(mid): -0.00000003
a: 1.41239071
b: 1.41239119
m: 1.41239119
--------------------
iterac: 22
f(a): 0.00000087
f(mid): 0.00000042
a: 1.41239095
b: 1.41239119
m: 1.41239095
--------------------
iterac: 23
f(a): 0.00000042
f(mid): 0.00000020
a: 1.41239107
b: 1.41239119
m: 1.41239107
--------------------
iterac: 24
f(a): 0.00000020
f(mid): 0.00000009
a: 1.41239113
b: 1.41239119
m: 1.41239113
--------------------
iterac: 25
f(a): 0.00000009
f(mid): 0.00000003
a: 1.41239116
b: 1.41239119
m: 1.41239116
--------------------
iterac: 26
f(a): 0.00000003
f(mid): 0.00000000
a: 1.41239117
b: 1.41239119
m: 1.41239117
--------------------
iterac: 27
f(a): 0.00000000
f(mid): -0.00000001
a: 1.41239117
b: 1.41239118
m: 1.41239118
--------------------


Intervalo [,] x''= 3.05710354 (aprox.)
No intervalo pedido na alternativa c eu acharia um erro de 0.05710355 entre a raiz calculada pelo algoritimo e a raiz numericamente aproximada da função naquele intervalo, no entanto eu aumentei um pouco o intervalo para achar uma raiz mais substancial.

iterac: 18
f(a): -0.00000922
f(mid): 0.00000441
a: 3.05709839
b: 3.05710602
m: 3.05710602
--------------------
iterac: 19
f(a): -0.00000922
f(mid): -0.00000241
a: 3.05710220
b: 3.05710602
m: 3.05710220
--------------------
iterac: 20
f(a): -0.00000241
f(mid): 0.00000100
a: 3.05710220
b: 3.05710411
m: 3.05710411
--------------------
iterac: 21
f(a): -0.00000241
f(mid): -0.00000070
a: 3.05710316
b: 3.05710411
m: 3.05710316
--------------------
iterac: 22
f(a): -0.00000070
f(mid): 0.00000015
a: 3.05710316
b: 3.05710363
m: 3.05710363
--------------------
iterac: 23
f(a): -0.00000070
f(mid): -0.00000028
a: 3.05710340
b: 3.05710363
m: 3.05710340
--------------------
iterac: 24
f(a): -0.00000028
f(mid): -0.00000006
a: 3.05710351
b: 3.05710363
m: 3.05710351
--------------------
iterac: 25
f(a): -0.00000006
f(mid): 0.00000004
a: 3.05710351
b: 3.05710357
m: 3.05710357
--------------------
iterac: 26
f(a): -0.00000006
f(mid): -0.00000001
a: 3.05710354
b: 3.05710357
m: 3.05710354
--------------------
iterac: 27
f(a): -0.00000001
f(mid): 0.00000002
a: 3.05710354
b: 3.05710356
m: 3.05710356
--------------------
iterac: 28
f(a): -0.00000001
f(mid): 0.00000000
a: 3.05710354
b: 3.05710355
m: 3.05710355
--------------------
*/
