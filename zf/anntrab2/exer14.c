#include <stdio.h>
#include <math.h>
//f(x)=x^3+x-4
//Exercício 14
main()
{
   double f_a,f_mid,a,b,m;
   int i=0;
   a=1;
   b=4;
   while ((b-a)>10e-9)
   {
      i++;
      m=(a+((b-a)/2));
      f_mid=((pow(m,3))+(m)-(4));
      f_a=((pow(a,3))+(a)-(4));
      if ((f_a*f_mid)>0)
      {
         a=m;
      }
      else
      {
         b=m;
      }
      printf ("iterac: %i\nf(a): %.8f\nf(mid): %.8f\na: %.8f\nb: %.8f\nm: %.8f\n--------------------\n",i,f_a,f_mid,a,b,m);
      if (m==1.37879801)
      {
         printf ("Limitante para a bisseccao iterac: %i\n",i);
      }
   }
};
/*
GCC OUTPUT
Não mostrei todos as iterações, por motivos de espaço, no entanto, ainda podemos visualizar como o programa trabalhou, partindo da 21º em diante. Nesse caso em especial eu coloquei um limitante como pede a questão, para quando o resultado ser de 1.37879801 (aprox.) ele pare naquela iteração

Intervalo [1,4] x'= 1.37879670 (aprox.)

iterac: 21
f(a): -0.00000082
f(mid): 0.00000877
a: 1.37879658
b: 1.37879801
m: 1.37879801
--------------------
iterac: 22
f(a): -0.00000082
f(mid): 0.00000397
a: 1.37879658
b: 1.37879729
m: 1.37879729
--------------------
iterac: 23
f(a): -0.00000082
f(mid): 0.00000157
a: 1.37879658
b: 1.37879694
m: 1.37879694
--------------------
iterac: 24
f(a): -0.00000082
f(mid): 0.00000038
a: 1.37879658
b: 1.37879676
m: 1.37879676
--------------------
iterac: 25
f(a): -0.00000082
f(mid): -0.00000022
a: 1.37879667
b: 1.37879676
m: 1.37879667
--------------------
iterac: 26
f(a): -0.00000022
f(mid): 0.00000008
a: 1.37879667
b: 1.37879671
m: 1.37879671
--------------------
iterac: 27
f(a): -0.00000022
f(mid): -0.00000007
a: 1.37879669
b: 1.37879671
m: 1.37879669
--------------------
iterac: 28
f(a): -0.00000007
f(mid): 0.00000000
a: 1.37879669
b: 1.37879670
m: 1.37879670
--------------------
iterac: 29
f(a): -0.00000007
f(mid): -0.00000004
a: 1.37879669
b: 1.37879670
m: 1.37879669
--------------------
*/
