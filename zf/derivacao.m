function coeff_derivative=derivate(coeff_function)
der_order=size((coeff_function),2)-1;
coeff_derivative=0;
for index=1:size((coeff_function),2)-1
    coeff_derivative(index)=der_order*coeff_function(index);
    der_order=der_order-1;
end
