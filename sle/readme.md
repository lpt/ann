# Lucas Tonussi

# Referências:

  *BAUDIN, Michaël. "Programming in Scilab" January 2011. Disponível em: <http://forge.scilab.org/index.php/p/docprogscilab/> Acesso em: 16 Maio 2011.*

  *FREITAS, Daniel S. "Decomposição LU de Doolittle", Cap. 3 - Sistemas lineares, Cap. 3.7 - Decomposição L-U de Doolittle. Universidade Federal de Santa Catarina, Depto de Informática e Estatística. Disponível em: <http://www.inf.ufsc.br/~santana/ine5232/material/cap3_sislin2.pdf> Acesso em: 16 Maio 2011.* [Material não disponível]

## Contato:
  * `tonussi@freenode`
  * `lptonussi@gmail.com`
  * `http://tonussi.github.com/`

## Licença:
  * [Creative Commons](http://creativecommons.org/licenses/by-nc-sa/3.0/)
