function cramer(m)
   a=rand(m,m)
   b=rand(m,1)
   x=a\b
   xc=cramer(a,b)
endfunction

function x=cramer(a,b)
   size(b)=[m n]
   z=zeros(m,1)
   ai=a;
   for k=1:m
     ai(:,k)=b
     z(k)=det(ai)
     ai(:,k)=a(:,k)
   end
   deta=det(a)
   x=z/deta
endfunction
//Tentativa de Regra de Cramer