/*
Dado o Sistema AX=B
            1
 A{ij} = -------
         (i+j+2)
                                /1\
                                |.|
Onde A é uma matriz 15x15 e B = |.|
                                |.|
                                \1/ {15x1}

Resolva X usando o método de Gauss-Jacobi e Gauss-Seidel, com erro 10^-7. Determine o número de iterações necessárias em cada caso.
Compare o resultado com a solução exata, obtida atravéz da eliminação de Gauss.

Repita o procedimento anterior para um sistema de equações com:
    /1.0 2.0 1.0 1.0 0.0\
    |3.0 7.0 0.3 1.1 2.1|
 A= |0.0 1.2 8.3 2.2 5.3|
    |2.5 2.7 0.7 1.8 2.1|
    \1.1 2.1 0.7 1.4 8.2/

     /1\
     |.|
 X = |.|
     |.|
     \1/

*/
