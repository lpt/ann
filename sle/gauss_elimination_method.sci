function x=gaussimples(a)
  n=size(a,1);
  m=size(a,2);
  for j=1:(n-1)
    pivo=a(j,j);
    per_line=%F;
    for i=j+1:n
      if abs(A(i,j))>2*abs(pivot) then
        pivo_line=i;
        pivo=A(i,j);
        per_line=%T;
      end
    end
    if per_line then
      temp_line=a(j,:);
      a(j,:)=A(pivo_line,:);
      a(pivo_line,:)=temp_line;
    end
    fator1=pivot;
    for i=j+1:n
      fator2=a(i,j);
      a(i,j)=0.;
      a(i,j+1:m)=(fator2/fator1)*a(j,j+1:m)-a(i,j+1:m);
    end
  end
  x(n,1:m-n)=a(n,n+1:m)/a(n,n);
  for j=n-1:-1:1
    x(j,1:m-n)=(a(j,n+1:m)-a(j,j+1:n)*x(j+1:n,1:m-n))/a(j,j);
  end
endfunction //endfunction x=gaussimples(a)