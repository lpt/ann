function h=hilb(n,n)
  h=zeros(n,n)
  for i=1:n;
    for j=i:n;
      h(i,j)=1/(i+j-1);
    end
  end
endfunction
function x=GaussPivotacao(a,b)
  n=size(a,"r");
  p=zeros(1,n-1);
  for k=1:n-1
    pos=k;
    pivo=abs(a(pos,k))
    for i=k:n
      if (abs(a(i,k))>pivo) then
        pos=i;
        pivo=abs(a(i,k));
      end
    end
    for j=k:n
      temp=a(k,j);
      a(k,j)=a(pos,j);
      a(pos,j)=temp;
    end
    p(k)=pos;
    for i=k+1:n
      a(i,k)=a(i,k)/a(k,k);
    end
    for i=k+1:n
      for j=k+1:n
        a(i,j)=a(i,j)-a(i,k)*a(k,j);
      end
    end
  end
  for k=1:n-1
    temp=b(k);
    pos=p(k);
    b(k)=b(pos);
    b(pos)=temp;
    for i=k+1:n
      b(i)=b(i)-b(k)*a(i,k);
    end
  end
  for k=n:-1:1
    t=0.
    for j=k+1:n
      t=t+a(k,j)*b(j);
    end
    b(k)=(b(k)-t)/a(k,k);
  end
  x=b;
endfunction


//Exercício 12-abc
  a=hilb(50);
  b=ones(50,1);
  x=GaussPivotacao(a,b)
  tic()
  TempoResolucao=GaussPivotacao(a,b);
  printf('\nTempo de Resolucao da CPU com a GaussPivotacao: %.3f\n',toc());
  tic()
  TempoResolucao=linsolve(a,b);
  printf('\nTempo de Resolucao da CPU com o linsolve: %.3f\n',toc());
  tic()
  TempoResolucao=a\b;
  printf('\nTempo de Resolucao da CPU com o backslash: %.3f\n',toc());
  //SCILAB OUTPUT
  //Tempo de Resolucao da CPU com a FunGaussPivotacao: 0.130 ms
  //Tempo de Resolucao da CPU com o linsolve: 0.009 ms
  //Tempo de Resolucao da CPU com o backslash: 0.002 ms
  
  
    //Magnitude
    a=hilb(50);
    e=ones(50,1);
    b=ones(50,1);
    xFuncaoImplementada=GaussPivotacao(a,b);
    log10(cond(a))
    
    //ans  = 2.4146668 
    
    
    norm(e-xFuncaoImplementada)/norm(e)
    
    
    //ans  = 13.948167  
    
    
    
    add_profiling("GaussPivotacao")
    xFuncaoImplementada=GaussPivotacao(a,b)
    plotprofile(GaussPivotacao)