function x=gausspivotacao(a,b)
  n=size(a,"r");
  p=zeros(1,n-1);
  for k=1:n-1
    pos=k;
    pivo=abs(a(pos,k))
    for i=k:n
      if (abs(a(i,k))>pivo) then
        pos=i;
        pivo=abs(a(i,k));
      end
    end
    for j=k:n
      temp=a(k,j);
      a(k,j)=a(pos,j);
      a(pos,j)=temp;
    end
    p(k)=pos;
    for i=k+1:n
      a(i,k)=a(i,k)/a(k,k);
    end
    for i=k+1:n
      for j=k+1:n
        a(i,j)=a(i,j)-a(i,k)*a(k,j);
      end
    end
  end
  for k=1:n-1
    temp=b(k);
    pos=p(k);
    b(k)=b(pos);
    b(pos)=temp;
    for i=k+1:n
      b(i)=b(i)-b(k)*a(i,k);
    end
  end
  for k=n:-1:1
    t=0.
    for j=k+1:n
      t=t+a(k,j)*b(j);
    end
    b(k)=(b(k)-t)/a(k,k);
  end
  x=b;
endfunction

//Exercicio1

// Necessidade da Utilização de add_profiling para a amostragem de Complexidade, CPU Time.
//   Pelos Documentos (CREATIVE COMMONS) de Michaël Baudin, Contribuidor do Scilab Consortium (R). Usei
//   de profiling para enxergar atravéz dos gráficos, nos vários níveis do código, que conforme o scilab
//   interpreta ele aumenta sei nível de complexidade, e reduz novamente, isso acompanha quase que igual-
//   mente (CPU Time of Process), ver gráficos. Que quando [code: a(i,j)=a(i,j)-a(i,k)*a(k,j);] ele tem
//   seu nível de complexidade mais alto. Eliminação de Gauss com Pivotação Parcial é bastante estudado
//   por Análistas de Software, pois exige bastante em Complexidade e Tempo de CPU. Logicamente, que 
//   trabalhando com Matrizes quase que absurdas, os gráficos ficariam muito mais evidentes. E expli-
//   cariam melhor o que se aplica.

n=4;
a=[2 7 9 5;-1 21 8 -4; 7 8 1 9; -4 7 9 1];
b=[1; 5; 1; 0];
xFuncaoImplementada=gausspivotacao(a,b)

//SCILAB OUTPUT xFuncaoImplementada=gausspivotacao(a,b)

// xScilabLinsolve =

//    0.3976157
//    0.1549790
//    0.0946704
//  - 0.3464236

//Comparando com linsolve
 
    a=[2 7 9 5;-1 21 8 -4;7 8 1 9;-4 7 9 1];
    b=[1;5;1;0];
    xScilabLinsolve=linsolve(a,-b)

// xScilabLinsolve =
//
//    0.3976157
//    0.1549790
//    0.0946704
//  - 0.3464236

 //Dois meios alternativos para a amostragem de tempo de CPU.

   //Configuração
   //Ubuntu 10.10 (maverick)
   //Kernel Linux 2.6.35-28-generic
   //GNOME 2.32.0
   //Memory 4096 Mhz
   //cat /proc/cpuinfo | grep processor | wc -l
   //2
   //Processor 0: Intel(R) Core(TM)2 Duo CPU E7500 @2.93Ghz
   //Processor 1: Intel(R) Core(TM)2 Duo CPU E7500 @2.93Ghz
   //Scilab

   //Primeito usando add_profiling()
   //Saída do Gráfico em imagem "cputime_ex1.png"
  
    n=4;
    a=[2 7 9 5;-1 21 8 -4; 7 8 1 9; -4 7 9 1];
    e=ones(n,1);
    b=[1; 5; 1; 0];
    xFuncaoImplementada=gausspivotacao(a,b);
    log10(cond(a))
    norm(e-xFuncaoImplementada)/norm(e) //magnitude
    //add_profiling("gausspivotacao")
    xFuncaoImplementada=gausspivotacao(a,b)
    //plotprofile(gausspivotacao)

  //Segundo usando Tic() Toc()

    a=[2 7 9 5;-1 21 8 -4;7 8 1 9;-4 7 9 1];
    b=[1;5;1;0];
    
    tic();
    TempoResolucao=gausspivotacao(a,b);
    printf('\nTempo de Resolucao da CPU com a Funcao Implementada: %.3f\n',toc());
    
    //Tempo de Resolucao com Funcao Implementada: 0.009 ms
    
    tic();
    TempoResolucao=linsolve(a,b);
    printf('\nTempo de Resolucao da CPU com o linsolve: %.3f\n',toc());
    
    //Tempo de Resolucao com linsolve: 0.001 ms