//Exercicio2
//Sistema [A]x=[B] para [L][U]x=[B]
//Decomposição da Matriz [A] em [L][U]

//Nas duas funções criadas DecomposicaoL e DecomposicaoU, eu utilizei de uma função do sistema
//para os resultados de L e U, e tentei de uma maneira criar funções para, usarem os resultados
//obtidos por [l,u]=lu(a). Então tentei resultados para x e xlinha, onde xlinha que multiplica b
//Contem um resultado não exato, mas aproximado. O por quê da DecomposicaoL (função), significa
//que é preciso calcular antes um x para obter um xlinha (com resultados) para x1..xi..xn; 

function x=DecomposicaoL(l,b,n)
  x=b;
  x(1)=b(1);
  for i=2:n
    somatorio=0;
    for j=1:i-1
      somatorio=somatorio+l(i,j)*x(j);
    end
    x(i)=b(i)-somatorio;
  end
endfunction
function xlinha=DecomposicaoU(u,x,n)
  xlinha=x;
  xlinha(n)=x(n)/u(n,n);
  for i=n-1:-1:1
    somatorio=0;
    for j=i+1:n
      somatorio=somatorio+u(i,j)*xlinha(j);
    end
    xlinha(i)=(x(i)-somatorio)/u(i,i);
  end
endfunction

n=4;
b=[1;5;1;0];
a=[2 7 9 5;-1 21 8 -4; 7 8 1 9; -4 7 9 1];
[l,u]=lu(a)
ResultadoParaX=(l*u)\b
x=DecomposicaoL(l,b,n)
xlinha=DecomposicaoU(u,x,n)


  //Diferença Temporal de Processamento das Funções Implementadas de DecomposicaoU() e DecomposicaoL()

tic();
TempoResolucao=DecomposicaoL(a,b);
printf('\nTempo de Resolucao da CPU com a Funcao Implementada: %.3f\n',toc());

    //Tempo de Resolucao da CPU com a Funcao Implementada: 0.001 ms

tic();
TempoResolucao=DecomposicaoU(a,b);
printf('\nTempo de Resolucao da CPU com a Funcao Implementada: %.3f\n',toc());

    //Tempo de Resolucao da CPU com a Funcao Implementada: 0.001 ms

    //Como é visivel, 0.001-0.001 = 0, mostra que para esse caso, as duas funções
    //desempenharam-se igualmente bem, visto que o tempo de processamento é o mesmo.

//SCILAB OUTPUT

// u  =
// 
//    7.    8.           1.           9.
//    0.    22.142857    8.1428571  - 2.7142857
//    0.    0.           6.9806452    3.0064516
//    0.    0.           0.           5.271719
// l  =
//
//    0.2857143    0.2129032    1.           0.
//  - 0.1428571    1.           0.           0.
//    1.           0.           0.           0.
//  - 0.5714286    0.5225806    0.7615527    1.
//
// ResultadoParaX=(l*u)\b
// ResultadoParaX  =
// 
//    0.3976157
//    0.1549790
//    0.0946704
//  - 0.3464236
//
// x=DecomposicaoL(l,b,n)
// x  =
// 
//    1.  
//    5.1428571
//    0.   
//  - 2.116129  
// xlinha=DecomposicaoU(u,x,n)
// xlinha  =
// 
//    0.4977152 
//    0.1194770 
//    0.1728815 
//  - 0.4014116
