function s=sassenfeld(a)
  [nl,nc]=size(a);
  n=nl;
  p=ones(1,n);
  for i=1:n
    somatorio=0;
    for j=1:n
      if j~=i then
        somatorio=somatorio+p(j)*abs(a(i,j))/abs(a(i,i));
      end
    end
    p(i)=somatorio;
  end
  s=0;
  for i=1:n
    if p(i)>s then
      s=p(i);
    end
  end
endfunction
function x=jacobi(a,b,tolerancia,iteracoes)
  n=size(a,1);
  x=zeros(n,1);
  for k=1:iteracoes
    y=x;
    for i=1:n
      somatorio=b(i);
      for j=1:n;
        if (j~=i)
          somatorio=somatorio-a(i,j)*y(j);
        end
      end
      x(i)=somatorio/a(i,i);
    end
    if (norm(x-y,1)<tolerancia)
      break
    end
  end
endfunction
function x=seidel(a)
  n=size(a,1);
  for i=1:n
    somatorio=0;
    for j=1:n
      if i~=j then
        somatorio=somatorio+abs(a(i,j));
      end
    end
    if somatorio>=abs(a(i,i)) then
      return 0;
    end
  end
  return -1;
endfunction
function x=gausssimples(a,b)
  n=size(a,1);
  x=zeros(n,1);
  for k=1:n-1
    for i=k+1:n
      mult=a(i,k)/a(k,k);
      a(i,k)=mult;
      for j=k+1:n
        a(i,j)=a(i,j)-mult*a(k,j);
      end
      b(i)=b(i)-mult*b(k);
    end
  end
  x(n)=b(n)/a(n,n);
  for i=n-1:-1:1
    x(i)=b(i);
    for j=i+1:n
      x(i)=x(i)-a(i,j)*x(j);
    end
    x(i)=x(i)/a(i,i);
  end
endfunction
function h=hilb(n,n)
  h=zeros(n,n)
  for i=1:n;
    for j=i:n;
      h(i,j)=1/(i+j+2); //formula da questão 17
    end
  end
endfunction
a=hilb(15);
b=ones(15,1);
tolerancia=10^-3
iteracoes=10^3
x=gausssimples(a,b)
s=sassenfeld(a)
x=jacobi(a,b,tolerancia,iteracoes)
x=seidel(a)

//Exercício 17
//
//    gauss simples
//-->x=gausssimples(a,b)
// x  =
// 
//  - 0.1020129  
//  - 0.1995823  
//  - 0.2978708  
//  - 0.3911628  
//  - 0.4773967  
//  - 0.556079   
//  - 0.6274100  
//  - 0.6918855  
//  - 0.7501080  
//  - 0.8026947  
//  - 0.8502352  
//  - 0.8932720  
//  - 0.9322951  
//  - 0.9677419  
//    32.        
//    
//    soma de sassenfeld    
//-->s=sassenfeld(a)
// s  =
// 
//    7.1639045  
//    
//    
//    gauss jacobi
//-->x=jacobi(a,b,tolerancia,iteracoes)
// x  =
// 
//  - 0.1020129  
//  - 0.1995823  
//  - 0.2978708  
//  - 0.3911628  
//  - 0.4773967  
//  - 0.556079   
//  - 0.6274100  
//  - 0.6918855  
//  - 0.7501080  
//  - 0.8026947  
//  - 0.8502352  
//  - 0.8932720  
//  - 0.9322951  
//  - 0.9677419  
//    32.     
//    
//    gauss seidel
//-->x=seidel(a)
// x  =
// 
//  - 0.1020129  
//  - 0.1995823  
//  - 0.2978708  
//  - 0.3911628  
//  - 0.4773967  
//  - 0.556079   
//  - 0.6274100  
//  - 0.6918855  
//  - 0.7501080  
//  - 0.8026947  
//  - 0.8502352  
//  - 0.8932720  
//  - 0.9322951  
//  - 0.9677419  
//    32.
//    
//    
//    backslash    
//-->a\b
// ans  =
// 
//  - 0.1020129  
//  - 0.1995823  
//  - 0.2978708  
//  - 0.3911628  
//  - 0.4773967  
//  - 0.556079   
//  - 0.6274100  
//  - 0.6918855  
//  - 0.7501080  
//  - 0.8026947  
//  - 0.8502352  
//  - 0.8932720  
//  - 0.9322951  
//  - 0.9677419  
//    32.
//
//