//Lucas Pagotto Tonussi
//Explicação código em SCILAB, tipo função x=gausspivotacao(a,b).

//Fonte desse pseudo-código, no qual usei para criar uma função de Gauss Simples http://en.wikipedia.org/wiki/Gauss_elimination
//while i<=m && j<=n do
//  Find pivot in column j, starting in row i:
//  maxi=i;
//  for k=i+1:m
//    if abs(a(k,j))>abs(a(maxi,j)) then
//     maxi=k;
//    end
//  end
//  if a(maxi,j)~=0 then
//    swap rows i and maxi, but do not change the value of i
//    Now A[i,j] will contain the old value of A[maxi,j].
//    divide each entry in row i by A[i,j]
//    Now A[i,j] will have the value 1.
//    for u=i+1:m
//    subtract A[u,j] * row i from row u
//    Now A[u,j] will be 0, since A[u,j] - A[i,j] * A[u,j] = A[u,j] - 1 * A[u,j] = 0.
//    end
//    i=i+1;
//  end
//  j=j+1;
//end

//para resolver no scilab sistemas Ax=b
//você já tem pré-estabelecido no scilab um arranjo dimensional dos sistemas que você coloca
//para que o scilab consiga resolver, no entanto, se nós precisarmos criar as funções no scilab
//para resolver por gauss com pivotação e retrosubstituição, precisaremos de uma função também
//que reconheça a dimensão, simplesmente segue no código nas primeiras linhas, essa função, bastante simples.
//n=size(a,"r"); seguindo de p=zeros(1,n-1). Este será nosso arranjo da matriz.
function x=gausspivotacao(a,b)
  n=size(a,"r");      //função size, é uma lista e retorna numero de elementos.
  p=zeros(1,n-1);     //arranjando a matriz logo após saber o número de elementos
  for k=1:n-1         //main-for para a eliminação de gauss simples
    pos=k;
    pivo=abs(a(pos,k))
    for i=k:n
      if (abs(a(i,k))>pivo) then //if para o bubble-sort da troca de linha
        pos=i;                   //guarda a posição
        pivo=abs(a(i,k));        //abs() funcao que me retorna o valor em módulo
      end                        //para escolha do maior pivô da coluna.
    end
    for j=k:n
      temp=a(k,j);      //temp sigla para temporario
      a(k,j)=a(pos,j);  //troca de linha
      a(pos,j)=temp;    //troca simples usando bubble-sort
    end
    p(k)=pos;
    for i=k+1:n
      a(i,k)=a(i,k)/a(k,k); //cálculo da "constante" que multiplicara a linha do maior elemento
    end
    for i=k+1:n
      for j=k+1:n
        a(i,j)=a(i,j)-a(i,k)*a(k,j); //multiplicação da linha pela "constante"
      end
    end
  end
  for k=1:n-1    //main-for da retro substituição
    temp=b(k);   //bubble-sort simples, para retro substituição
    pos=p(k);    //onde eu guardo na variável [temp]orária, como de costume.
    b(k)=b(pos); //trocando b(k) e b(p(k))
    b(pos)=temp;
    for i=k+1:n
      b(i)=b(i)-b(k)*a(i,k);
    end
  end
  for k=n:-1:1
    t=0.
    for j=k+1:n
      t=t+a(k,j)*b(j); //Finalizando a retrosubstituição.
    end
    b(k)=(b(k)-t)/a(k,k);
  end
  x=b; //Aplicando os valores para x, resultantes das convergências das iterações do código.
endfunction

//Referências Bibliográficas

//BAUDIN, Michaël. "Programming in Scilab" January 2011. Disponível em: <http://forge.scilab.org/index.php/p/docprogscilab/> Acesso em: 16 Maio 2011.
