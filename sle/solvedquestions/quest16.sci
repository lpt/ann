function s=sassenfeld(a)
  [nl,nc]=size(a);
  n=nl;
  p=ones(1,n);
  for i=1:n
    somatorio=0;
    for j=1:n
      if j~=i then
        somatorio=somatorio+p(j)*abs(a(i,j))/abs(a(i,i));
      end
    end
    p(i)=somatorio;
  end
  s=0;
  for i=1:n
    if p(i)>s then
      s=p(i);
    end
  end
endfunction
function x=jacobi(a,b,tolerancia,iteracoes)
  n=size(a,1);
  x=zeros(n,1);
  for k=1:iteracoes
    y=x;
    for i=1:n
      somatorio=b(i);
      for j=1:n;
        if (j~=i)
          somatorio=somatorio-a(i,j)*y(j);
        end
      end
      x(i)=somatorio/a(i,i);
    end
    if (norm(x-y,1)<tolerancia)
      break
    end
  end
endfunction
function x=seidel(a)
  n=size(a,1);
  for i=1:n
    somatorio=0;
    for j=1:n
      if i~=j then
        somatorio=somatorio+abs(a(i,j));
      end
    end
    if somatorio>=abs(a(i,i)) then
      return 0;
    end
  end
  return -1;
endfunction
function x=gausssimples(a,b)
  n=size(a,1);
  x=zeros(n,1);
  for k=1:n-1
    for i=k+1:n
      mult=a(i,k)/a(k,k);
      a(i,k)=mult;
      for j=k+1:n
        a(i,j)=a(i,j)-mult*a(k,j);
      end
      b(i)=b(i)-mult*b(k);
    end
  end
  x(n)=b(n)/a(n,n);
  for i=n-1:-1:1
    x(i)=b(i);
    for j=i+1:n
      x(i)=x(i)-a(i,j)*x(j);
    end
    x(i)=x(i)/a(i,i);
  end
endfunction
a=[1 2 1 1 0; 3 7 0.3 1.1 2.1; 0 1.2 8.3 2.2 5.3; 2.5 2.7 0.7 9.8 2.1; 1.1 2.1 0.7 1.5 8.2]
b=ones(5,1)
tolerancia=10^-8
iteracoes=100
x=gausssimples(a,b)
s=sassenfeld(a)
x=jacobi(a,b,tolerancia,iteracoes)
x=seidel(a)
norm(x,'inf')
norm(x)
norm(a,'inf')
norm(a)

// Normas
//-->norm(x,'inf')
// ans  =
// 
//    4.862D+12  
//-->norm(x)
// ans  =
// 
//    5.694D+12  
//-->norm(a,'inf')
// ans  =
// 
//    17.8  
//-->norm(a)
// ans  =
// 
//    14.759301


//Exercício 16
//-->a=[1 2 1 1 0; 3 7 0.3 1.1 2.1; 0 1.2 8.3 2.2 5.3; 2.5 2.7 0.7 9.8 2.1; 1.1 2.1 0.7 1.5 8.2]
// a  =
// 
//    1.     2.     1.     1.     0.   
//    3.     7.     0.3    1.1    2.1  
//    0.     1.2    8.3    2.2    5.3  
//    2.5    2.7    0.7    9.8    2.1  
//    1.1    2.1    0.7    1.5    8.2  
//-->b=ones(5,1)
// b  =
// 
//    1.  
//    1.  
//    1.  
//    1.  
//    1.  
//-->tolerancia=10^-8
// tolerancia  =
// 
//    1.000D-08  
//-->iteracoes=100
// iteracoes  =
// 
//    100.  
//-->x=gausssimples(a,b)
// x  =
// 
//    4.7667817  
//  - 1.8276668  
//    0.5428890  
//  - 0.6543370  
//    0.0239175  
//-->s=sassenfeld(a)
// s  =
// 
//    4.  
//-->x=jacobi(a,b,tolerancia,iteracoes)
// x  =
// 
//   1.0D+12 *
// 
//  - 4.861715   
//  - 1.9983473  
//  - 1.049101   
//  - 1.5533568  
//  - 1.1328228  
//-->x=seidel(a)
// x  =
// 
//   1.0D+12 *
// 
//  - 4.861715   
//  - 1.9983473  
//  - 1.049101   
//  - 1.5533568  
//  - 1.1328228