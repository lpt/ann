//Meios alternativos para a visualizacao do tempo de processamento.sci

  //Configuração
  //Ubuntu 10.10 (maverick)
  //Kernel Linux 2.6.35-28-generic
  //GNOME 2.32.0
  //Memory 4096 Mhz
  //cat /proc/cpuinfo | grep processor | wc -l
  //2
  //Processor 0: Intel(R) Core(TM)2 Duo CPU E7500 @2.93Ghz
  //Processor 1: Intel(R) Core(TM)2 Duo CPU E7500 @2.93Ghz
  //Scilab
  
  //Via add_profiling()

    //n=4;
    //a=[2 7 9 5;-1 21 8 -4; 7 8 1 9; -4 7 9 1];
    //e=ones(n,1);
    //b=[1; 5; 1; 0];
    //xFuncaoImplementada=gausspivotacao(a,b);
    //log10(cond(a))
    //norm(e-xFuncaoImplementada)/norm(e)
    //add_profiling("gausspivotacao")
    //xFuncaoImplementada=gausspivotacao(a,b)
    //plotprofile(gausspivotacao)

  //Via Tic() Toc()

    //a=[2 7 9 5;-1 21 8 -4;7 8 1 9;-4 7 9 1];
    //b=[1;5;1;0];
    
    //tic();
    //TempoResolucao=gausspivotacao(a,b);
    //printf('\nTempo de Resolucao da CPU com a Funcao Implementada: %.3f\n',toc());
    
    //Tempo de Resolucao com Funcao Implementada: 0.009
    
    //tic();
    //TempoResolucao=linsolve(a,b);
    //printf('\nTempo de Resolucao da CPU com o linsolve: %.3f\n',toc());
    
    //Tempo de Resolucao com linsolve: 0.001
