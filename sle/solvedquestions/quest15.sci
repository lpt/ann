function s=sassenfeld(a)
  [nl,nc]=size(a);
  n=nl;
  p=ones(1,n);
  for i=1:n
    somatorio=0;
    for j=1:n
      if j~=i then
        somatorio=somatorio+p(j)*abs(a(i,j))/abs(a(i,i));
      end
    end
    p(i)=somatorio;
  end
  s=0;
  for i=1:n
    if p(i)>s then
      s=p(i);
    end
  end
endfunction
function x=jacobi(a,b,tolerancia,iteracoes)
  n=size(a,1);
  x=zeros(n,1);
  for k=1:iteracoes
    y=x;
    for i=1:n
      somatorio=b(i);
      for j=1:n;
        if (j~=i)
          somatorio=somatorio-a(i,j)*y(j);
        end
      end
      x(i)=somatorio/a(i,i);
    end
    if (norm(x-y,1)<tolerancia)
      break
    end
  end
endfunction
function x=seidel(a)
  n=size(a,1);
  for i=1:n
    somatorio=0;
    for j=1:n
      if i~=j then
        somatorio=somatorio+abs(a(i,j));
      end
    end
    if somatorio>=abs(a(i,i)) then
      return 0;
    end
  end
  return -1;
endfunction
function x=gausssimples(a,b)
  n=size(a,1);
  x=zeros(n,1);
  for k=1:n-1
    for i=k+1:n
      mult=a(i,k)/a(k,k);
      a(i,k)=mult;
      for j=k+1:n
        a(i,j)=a(i,j)-mult*a(k,j);
      end
      b(i)=b(i)-mult*b(k);
    end
  end
  x(n)=b(n)/a(n,n);
  for i=n-1:-1:1
    x(i)=b(i);
    for j=i+1:n
      x(i)=x(i)-a(i,j)*x(j);
    end
    x(i)=x(i)/a(i,i);
  end
endfunction
a=[5.1 0.4 1.8 -3.4 -1.9; -1.5 0.4 0.1 0.6 -1.1; 0.4 0.3 22.5 -1.3 0.6; 1.4 -0.3 -0.2 5.4 -2.4; 0.4 0.1 0.8 1.4 5.7]
b=[-4.2;-3.6;-5.4;0.3;-6.2;4.4]
size(a)
size(b)
tolerancia=10^-8
iteracoes=100
x=gausssimples(a,b)
s=sassenfeld(a)
x=jacobi(a,b,tolerancia,iteracoes)
x=seidel(a)
a\b
linsolve(a,b)



//Exercício 15
//-->a=[5.1 0.4 1.8 -3.4 -1.9; -1.5 0.4 0.1 0.6 -1.1; 0.4 0.3 22.5 -1.3 0.6; 1.4 -0.3 -0.2 5.4 -2.4; 0.4 0.1 0.8 1.4 5.7]
// a  =
// 
//    5.1    0.4    1.8   - 3.4  - 1.9  
//  - 1.5    0.4    0.1     0.6  - 1.1  
//    0.4    0.3    22.5  - 1.3    0.6  
//    1.4  - 0.3  - 0.2     5.4  - 2.4  
//    0.4    0.1    0.8     1.4    5.7  
//-->b=[-4.2;-3.6;-5.4;0.3;-6.2;4.4]
// b  =
// 
//  - 4.2  
//  - 3.6  
//  - 5.4  
//    0.3  
//  - 6.2  
//    4.4  
//-->tolerancia=10^-8
// tolerancia  =
// 
//    1.000D-08  
//-->iteracoes=100
// iteracoes  =
// 
//    100.  
//-->x=gausssimples(a,b)
// x  =
// 
//  - 0.5917804  
//  - 11.854873  
//  - 0.0969651  
//  - 0.7390309  
//  - 0.6430851  
//-->s=sassenfeld(a)
// s  =
// 
//    10.014706  
//-->x=jacobi(a,b,tolerancia,iteracoes)
// x  =
// 
//  - 0.5917804  
//  - 11.854873  
//  - 0.0969651  
//  - 0.7390309  
//  - 0.6430851  
//-->x=seidel(a)
// x  =
// 
//  - 0.5917804  
//  - 11.854873  
//  - 0.0969651  
//  - 0.7390309  
//  - 0.6430851