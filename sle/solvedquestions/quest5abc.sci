function x=FuncaoJacobi(a,b,tolerancia,iteracoes)
  n=size(a,1);
  x=zeros(n,1);
  for k=1:iteracoes
    y=x;
    for i=1:n
      somatorio=b(i);
      for j=1:n;
        if (j~=i)
          somatorio=somatorio-a(i,j)*y(j);
        end
      end
      x(i)=somatorio/a(i,i);
    end
    if (norm(x-y,1)<tolerancia)
      break
    end
  end
endfunction
function x=gausspivotacao(a,b)
  n=size(a,"r");
  p=zeros(1,n-1);
  for k=1:n-1
    pos=k;
    pivo=abs(a(pos,k))
    for i=k:n
      if (abs(a(i,k))>pivo) then
        pos=i;
        pivo=abs(a(i,k));
      end
    end
    for j=k:n
      temp=a(k,j);
      a(k,j)=a(pos,j);
      a(pos,j)=temp;
    end
    p(k)=pos;
    for i=k+1:n
      a(i,k)=a(i,k)/a(k,k);
    end
    for i=k+1:n
      for j=k+1:n
        a(i,j)=a(i,j)-a(i,k)*a(k,j);
      end
    end
  end
  for k=1:n-1
    temp=b(k);
    pos=p(k);
    b(k)=b(pos);
    b(pos)=temp;
    for i=k+1:n
      b(i)=b(i)-b(k)*a(i,k);
    end
  end
  for k=n:-1:1
    t=0.
    for j=k+1:n
      t=t+a(k,j)*b(j);
    end
    b(k)=(b(k)-t)/a(k,k);
  end
  x=b;
endfunction
function x=GaussSimples(a,b,n)
  x=zeros(n,1); //Ax=b :.. x=A\b
  for k=1:n-1
    for i=k+1:n
      mult=a(i,k)/a(k,k);
      a(i,k)=mult;
      for j=k+1:n
        a(i,j)=a(i,j)-mult*a(k,j); //eliminação
      end
      b(i)=b(i)-mult*b(k);
    end
  end
  x(n)=b(n)/a(n,n);
  for i=n-1:-1:1
    x(i)=b(i);
    for j=i+1:n
      x(i)=x(i)-a(i,j)*x(j); //substituição
    end
    x(i)=x(i)/a(i,i);
  end
endfunction


//SCILAB OUTPUT
//
//Exercício 5 (i)

n=4;
a=[1.545 1.543 1.432 1.542; 1.453 1.476 1.512 1.432; 1.453 1.433 1.491 1.545; 1.454 1.534 1.435 1.511];
b=[1.464; 1.462; 1.463; 1.460];
x=GaussSimples(a,b,n)
xComparativo=a\b

//Via Eliminação de Gauss sem Pivotação
//
// x =
// 
//  - 0.0466421
//    0.3179425
//    0.4918454
//    0.2212414
//
//Comparando com a\b (SCILAB) e linsolve(a,b)
//
// xComparativo =
//
//   - 0.0466421
//     0.3179425
//     0.4918454
//     0.2212414


  //Tempo de Resolução da CPU (ms)
    tic();
    TempoResolucao=GaussSimples(a,b,n);
    printf('\nTempo de Resolucao da CPU com a Funcao Implementada: %.3f\n',toc());
    
    //Tempo de Resolucao com Funcao Implementada: 0.001 ms
    
    tic();
    TempoResolucao=linsolve(a,b);
    printf('\nTempo de Resolucao da CPU com o linsolve: %.3f\n',toc());

    //Tempo de Resolucao com Funcao linsolve: 0.001 ms
    
    
//Exercicio 5 (ii)
//

a=[1.545 1.543 1.432 1.542; 1.453 1.476 1.512 1.432; 1.453 1.433 1.491 1.545; 1.454 1.534 1.435 1.511];
b=[1.464; 1.462; 1.463; 1.460];
x=gausspivotacao(a,b)
tolerancia=6;
iteracoes=100;
x=FuncaoJacobi(a,b,tolerancia,iteracoes)



//Exercício 5 (iii)

//Não consegui entender como fazer isso no scilab.
    
