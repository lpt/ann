//Exercicio4
//Sistema [A]x=[B] para [L][U]x=[B]
//Decomposição da Matriz [A] em [L][U]

//Nas duas funções criadas DecomposicaoL e DecomposicaoU, eu utilizei de uma função do sistema
//para os resultados de L e U, e tentei de uma maneira criar funções para, usarem os resultados
//obtidos por [l,u]=lu(a). Então tentei resultados para x e xlinha, onde xlinha que multiplica b
//Contem um resultado não exato, mas aproximado. O por quê da DecomposicaoL (função), significa
//que é preciso calcular antes um x para obter um xlinha (com resultados) para x1..xi..xn; 

function x=DecomposicaoL(l,b,n)
  x=b;
  x(1)=b(1);
  for i=2:n
    somatorio=0;
    for j=1:i-1
      somatorio=somatorio+l(i,j)*x(j);
    end
    x(i)=b(i)-somatorio;
  end
endfunction

function xlinha=DecomposicaoU(u,x,n)
  xlinha=x;
  xlinha(n)=x(n)/u(n,n);
  for i=n-1:-1:1
    somatorio=0;
    for j=i+1:n
      somatorio=somatorio+u(i,j)*xlinha(j);
    end
    xlinha(i)=(x(i)-somatorio)/u(i,i);
  end
endfunction

n=8;
b=ones(n,1);
a=[10 -2 -1 2 3 1 -4 7; 5 11 3 10 -3 3 3 -4; 7 12 1 5 3 -12 2 3; 5 7 3 -2 3 2 2 4; 9 -15 6 5 4 -1 8 3; 1 2 5 -1 12 -1 4 1; 3 3 3 7 1 1 -1 -3; -1 3 4 1 3 -4 7 6];
[l,u]=lu(a)
ResultadoParaX=(l*u)\b
x=DecomposicaoL(l,b,n)
xlinha=DecomposicaoU(u,x,n)



//SCILAB OUTPUT


//AX=B [A]=>[L][U]
//LUx=B