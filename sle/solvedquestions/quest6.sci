function x=GaussComPivotacao(a,b)
  n=size(a,"r");
  p=zeros(1,n-1);
  for k=1:n-1
    pos=k;
    pivo=abs(a(pos,k))
    for i=k:n
      if (abs(a(i,k))>pivo) then
        pos=i;
        pivo=abs(a(i,k));
      end
    end
    for j=k:n
      temp=a(k,j);
      a(k,j)=a(pos,j);
      a(pos,j)=temp;
    end
    p(k)=pos;
    for i=k+1:n
      a(i,k)=a(i,k)/a(k,k);
    end
    for i=k+1:n
      for j=k+1:n
        a(i,j)=a(i,j)-a(i,k)*a(k,j);
      end
    end
  end
  for k=1:n-1
    temp=b(k);
    pos=p(k);
    b(k)=b(pos);
    b(pos)=temp;
    for i=k+1:n
      b(i)=b(i)-b(k)*a(i,k);
    end
  end
  for k=n:-1:1
    t=0.
    for j=k+1:n
      t=t+a(k,j)*b(j);
    end
    b(k)=(b(k)-t)/a(k,k);
  end
  x=b;
endfunction

//Exercicio 6

//  3.90x + 0.013y = 35.2
// -6.32x + 5.430y = 34.5

//Comparandom com a Função Implementada de Eliminaçao com Pivotação.

a=[3.9 0.013;-6.32 5.43]
b=[35.2;34.5]
x=GaussComPivotacao(a,b)

//Comparando com os resultados para x

X1=1205.145/134.35789  //Calculado Manualmente
X2=357.014/21.25916    //Calculado Manualmente

//
//  /-6.32 5.430\ x /X1\ = /34.5\  ... -3.9/6.32 x (-6.32 5.430)(34.5) ... (3.9 -21.177/6.32) (-134.55/6.32)
//  \ 3.90 0.013/   \X2/   \35.2/
//  [l2 trocou l1]
//
//  /-6.32      5.430   \ x /X1\ = /    34.5    \   ...    X2 = (357.014/21.25916) ... X1 = (1205.145/134.35789)
//  \  0   21.25916/6.32/   \X2/   \357.014/6.32/   [feito diminuição das linhas temos]


//Exercicio 7

//  0.01x + 2.0y + 1z = 15.0
//  3.00x + 2.0y + 3z =  6.3
//  5.00x + 4.4y + 5z = 46.0
//  Número de Passos = Dimensão - 1 .:. 3 - 1 = 2
//
//
//1/100   2    1 | 15                      5   22/5 5 | 46
//    3   2    3 | 63/10                   3    2   3 | 63/10
//    5   22/5 5 | 46                      1/100 2   1 | 15        
//        [A]      [B]                     [l2 trocou l1]
//         
//    5  22/5   5 |  46                    5    22/5      5     |  46
//    0 -16/25  0 | -213/10                0   -16/25     0     | -213/10
//    5  22/5   5 |  46                    0  2489/1250 99/100  |  3727/250
//    
//    5   22/5      5     |  46                /   6295/198 \
//    0 2489/1250 99/100  |  3727/250      x = |   1065/32  |
//    0     0    792/2489 | -41093/24890       \-410893/7920/ [feito pivotação das linhas temos os seguintes resultados]

//Comparandom com a Função Implementada de Eliminaçao com Pivotação.


a=[0.01 2 1; 3 2 3; 5 4.4 5];
b=[15; 6.3; 46];
x=GaussComPivotacao(a,b)

//Comparando com os resultados para x


X1 = 6295/198     // Calculado Manualmente
X2 = 1065/32      // Calculado Manualmente
X3 = -410893/7920 // Calculado Manualmente