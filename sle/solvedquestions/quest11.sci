//                 1 
//  H(i,j) =  -----------
//             i + j - 1
//  H(i,j) = ∫¹ [x^(i+j-1) dx] "Integral de 0 à 1 de x^(i+j-1) dx"
//           ⁰    
//  Solving: ∫¹ (x^a)dx :. x^(a+1) |¹               :..   x^(i+j) |¹  :...    1        (i+j)^-1 
//           ⁰             ------- |  (i+j-1) is a        ------- |         -----  or  ---------
//                           a+1   ⁰                        i+j   ⁰          i+j           1
//  i.e: 5x5 Matrix of Hilbert
//  1/1 1/2 1/3 1/4 1/5
//  1/2 1/3 1/4 1/5 1/6
//  1/3 1/4 1/5 1/6 1/7
//  1/4 1/5 1/6 1/7 1/8
//  1/5 1/6 1/7 1/8 1/9
function x=gausspivotacao(a,b)
  n=size(a,"r");
  p=zeros(1,n-1);
  for k=1:n-1
    pos=k;
    pivo=abs(a(pos,k))
    for i=k:n
      if (abs(a(i,k))>pivo) then
        pos=i;
        pivo=abs(a(i,k));
      end
    end
    for j=k:n
      temp=a(k,j);
      a(k,j)=a(pos,j);
      a(pos,j)=temp;
    end
    p(k)=pos;
    for i=k+1:n
      a(i,k)=a(i,k)/a(k,k);
    end
    for i=k+1:n
      for j=k+1:n
        a(i,j)=a(i,j)-a(i,k)*a(k,j);
      end
    end
  end
  for k=1:n-1
    temp=b(k);
    pos=p(k);
    b(k)=b(pos);
    b(pos)=temp;
    for i=k+1:n
      b(i)=b(i)-b(k)*a(i,k);
    end
  end
  for k=n:-1:1
    t=0.
    for j=k+1:n
      t=t+a(k,j)*b(j);
    end
    b(k)=(b(k)-t)/a(k,k);
  end
  x=b;
endfunction
function h=hilb(n,n)
  h=zeros(n,n)
  for i=1:n;
    for j=i:n;
      h(i,j)=1/(i+j-1);
    end
  end
endfunction
//Exercício 11-a
//
h=hilb(10)
a=[1/2 1/3 1/4 1/5 1/6 1/7 1/8 1/9 1/10 1/11; 1/3 1/4 1/5 1/6 1/7 1/8 1/9 1/10 1/11 1/12; 1/4 1/5 1/6 1/7 1/8 1/9 1/10 1/11 1/12 1/13; 1/5 1/6 1/7 1/8 1/9 1/10 1/11 1/12 1/13 1/14; 1/6 1/7 1/8 1/9 1/10 1/11 1/12 1/13 1/14 1/15; 1/7 1/8 1/9 1/10 1/11 1/12 1/13 1/14 1/15 1/16; 1/8 1/9 1/10 1/11 1/12 1/13 1/14 1/15 1/16 1/17; 1/9 1/10 1/11 1/12 1/13 1/14 1/15 1/16 1/17 1/18; 1/10 1/11 1/12 1/13 1/14 1/15 1/16 1/17 1/18 1/19; 1/11 1/12 1/13 1/14 1/15 1/16 1/17 1/18 1/19 1/20]
b=[1;1/2;1/3;1/4;1/5;1/6;1/7;1/8;1/9;1/10]
stacksize();
linsolve(a,b)
// ans  =
// 
//  - 29.329785  
//    190.71352  
//  - 397.40714  
//    128.33488  
//    259.68649  
//    77.566298  
//  - 135.11491  
//  - 211.18865  
//  - 94.020729  
//    210.90729
a\b
// Resposta do sistema SCILAB para o uso de a\b
// Warning: Matrix is close to singular or badly scaled.
// rcond = 7.5277D-15 computing least squares solution. (see lsq).
// ans  =
// 
//    45.473709  
//  - 493.77254  
//    2111.9963  
//  - 4012.4594  
//    2946.7342  
//    0.         
//    0.         
//  - 969.18206  
//    0.         
//    373.02466
x=gausspivotacao(a,b)
// x  =
// 
//    110.00924  
//  - 2970.4406  
//    34326.836  
//  - 210260.54  
//    756963.79  
//  - 1682190.   
//    2334525.   
//  - 1969797.1  
//    924119.41  
//  - 184826.88

//Exercício 11-b

//Exercício 11-c
//Diferenças

//linsolve(a,b)                               259.68649-(-397.40714) = 657.09363

//a\b                                         373.02466-(-4012.4594) = 4385.48406

//x=gausspivotacao(a,b), Função Implementada, 924119.41-(-2970.4406) = 927089.8506

//Calculando a Norma
norm(a,'inf')
//
// ans  =
// 
//    2.0198773  
//    
norm(a)
//
// ans  =
// 
//    1.2876627