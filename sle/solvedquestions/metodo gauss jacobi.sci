function x=FuncaoJacobi(a,b,tolerancia,iteracoes)
  n=size(a,1);
  x=zeros(n,1);
  for k=1:iteracoes //A Variável "perfeicao". Tem a funcao de conter o número de iterações
    y=x;            //para que o algoritimo que eu fiz, chegue num bom valor para x1,..xi..,xn.
    for i=1:n
      somatorio=b(i);
      for j=1:n;
        if (j~=i)
          somatorio=somatorio-a(i,j)*y(j);
        end
      end
      x(i)=somatorio/a(i,i);
    end
    if (norm(x-y,1)<tolerancia) //norma de parada
      break
    end
  end
endfunction

a=[5 0 0 -3 -1; -1 4 0 0 -1; 0 0 2 -1 0; -1 0 0 4 -2; 0 0 0 -1 2]
b=[2; 3; -1; 0; -1]
a\b
tolerancia=10^-8; //Aumento da Tolerância, aumenta precisão, e congergência para algum x1,x2,...,xn melhor.
iteracoes=1000; //Aumento de Iterações,  aumenta precisão , e congergência para algum x1,x2,...,xn melhor.
x=FuncaoJacobi(a,b,tolerancia,iteracoes)

  //Tempo de CPU
  
  tic()
  TempoResolucao=FuncaoJacobi(a,b,tolerancia,iteracoes);
  printf('\nTempo de Resolucao da CPU com a Funcao Implementada: %.3f\n',toc());
  
     //Tempo de Resolucao da CPU com a Funcao Implementada: 0.003 ms