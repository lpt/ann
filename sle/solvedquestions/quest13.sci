function x=jacobi(a,b,tolerancia,iteracoes)
  n=size(a,1);
  x=zeros(n,1);
  for k=1:iteracoes
    y=x;
    for i=1:n
      somatorio=b(i);
      for j=1:n;
        if (j~=i)
          somatorio=somatorio-a(i,j)*y(j);
        end
      end
      x(i)=somatorio/a(i,i);
    end
    if (norm(x-y,1)<tolerancia)
      break
    end
  end
endfunction
function x=seidel(a)
  n=size(a,1);
  for i=1:n
    somatorio=0;
    for j=1:n
      if i~=j then
        somatorio=somatorio+abs(a(i,j));
      end
    end
    if somatorio>=abs(a(i,i)) then
      return 0;
    end
  end
  return -1;
endfunction

a=[1 -1/2; 1 1]
b=[1/2; 5]
tolerancia=0.00001
iteracoes=10000
x=jacobi(a,b,tolerancia,iteracoes)
a\b
x=seidel(a)

//Nota: Utilizei troca de linhas, para passar no critério pois evidentemente, na hora de convergir para valores de x, os resultados não satizfaziam. Então foi preciso a troca. E não consegui plotar graficamente, como no Maple.
//Exercício 13
//  a=[1 -1/2; 1 1]
// a  =
// 
//    1.  - 0.5  
//    1.    1.   
//  b=[1/2; 5]
// b  =
// 
//    0.5  
//    5.   
//  tolerancia=0.00001
// tolerancia  =
// 
//    0.00001  
//  iteracoes=10000
// iteracoes  =
// 
//    10000.  
//  x=jacobi(a,b,tolerancia,iteracoes)
// x  =
// 
//    1.9999981  
//    2.9999971  
//  a\b
// ans  =
// 
//    2.  
//    3.  
//  x=seidel(a)
// x  =
// 
//    1.9999981  
//    2.9999971
