function s=sassenfeld(a)
  [nl,nc]=size(a);
  n=nl;
  p=ones(1,n);
  for i=1:n
    somatorio=0;
    for j=1:n
      if j~=i then
        somatorio=somatorio+p(j)*abs(a(i,j))/abs(a(i,i));
      end
    end
    p(i)=somatorio;
  end
  s=0;
  for i=1:n
    if p(i)>s then
      s=p(i);
    end
  end
endfunction
function x=jacobi(a,b,tolerancia,iteracoes)
  n=size(a,1);
  x=zeros(n,1);
  for k=1:iteracoes
    y=x;
    for i=1:n
      somatorio=b(i);
      for j=1:n;
        if (j~=i)
          somatorio=somatorio-a(i,j)*y(j);
        end
      end
      x(i)=somatorio/a(i,i);
    end
    if (norm(x-y,1)<tolerancia)
      break
    end
  end
endfunction
function x=seidel(a)
  n=size(a,1);
  for i=1:n
    somatorio=0;
    for j=1:n
      if i~=j then
        somatorio=somatorio+abs(a(i,j));
      end
    end
    if somatorio>=abs(a(i,i)) then
      return 0;
    end
  end
  return -1;
endfunction
function x=gausssimples(a,b)
  n=size(a,1);
  x=zeros(n,1);
  for k=1:n-1
    for i=k+1:n
      mult=a(i,k)/a(k,k);
      a(i,k)=mult;
      for j=k+1:n
        a(i,j)=a(i,j)-mult*a(k,j);
      end
      b(i)=b(i)-mult*b(k);
    end
  end
  x(n)=b(n)/a(n,n);
  for i=n-1:-1:1
    x(i)=b(i);
    for j=i+1:n
      x(i)=x(i)-a(i,j)*x(j);
    end
    x(i)=x(i)/a(i,i);
  end
endfunction


//Exercício 14
a=[4.7 3.1 8.1 -11.6; 5.2 7.3 1.1 2.5; 14.2 4.1 -6.2 1.3; 1.5 4.4 -21.2 4.1]
b=[2.2; 3.2; -3.7; 4.9]
tolerancia=10^-4
iteracoes=300
x=jacobi(a,b,tolerancia,iteracoes)
x=gausssimples(a,b)
s=sassenfeld(a)
linsolve(a,b)
norm(x,'inf')
norm(x)
norm(a,'inf')
norm(a)

// Normas
//-->norm(x,'inf')
// ans  =
// 
//    0.9453321  
//-->norm(x)
// ans  =
// 
//    1.1362213  
//-->norm(a,'inf')
// ans  =
// 
//    31.2  
//-->norm(a)
// ans  =
// 
//    25.658008


//-->x=jacobi(a,b,tolerancia,iteracoes)
// Caso eu aumente muito a tolerância e o numero de iteracões
// Saem resultados para x = [Nan Nan Nan Nan]
// x  =
// 
//   1.0D+54 *
// 
//    45.336755  
//    5.7170872  
//  - 33.070987  
//  - 1.0272223  
//-->x=gausssimples(a,b)
// x  =
// 
//  - 0.5644077  
//    0.9453321  
//  - 0.1236045  
//  - 0.2520158  
//-->s=sassenfeld(a)
// s  =
// 
//    78.048034  
//-->linsolve(a,b)
// ans  =
// 
//    0.5644077  
//  - 0.9453321  
//    0.1236045  
//    0.2520158 