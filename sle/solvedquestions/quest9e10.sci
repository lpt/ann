function x=gausspivotacao(a,b)
  n=size(a,"r");
  p=zeros(1,n-1);
  for k=1:n-1
    pos=k;
    pivo=abs(a(pos,k))
    for i=k:n
      if (abs(a(i,k))>pivo) then
        pos=i;
        pivo=abs(a(i,k));
      end
    end
    for j=k:n
      temp=a(k,j);
      a(k,j)=a(pos,j);
      a(pos,j)=temp;
    end
    p(k)=pos;
    for i=k+1:n
      a(i,k)=a(i,k)/a(k,k);
    end
    for i=k+1:n
      for j=k+1:n
        a(i,j)=a(i,j)-a(i,k)*a(k,j);
      end
    end
  end
  for k=1:n-1
    temp=b(k);
    pos=p(k);
    b(k)=b(pos);
    b(pos)=temp;
    for i=k+1:n
      b(i)=b(i)-b(k)*a(i,k);
    end
  end
  for k=n:-1:1
    t=0.
    for j=k+1:n
      t=t+a(k,j)*b(j);
    end
    b(k)=(b(k)-t)/a(k,k);
  end
  x=b;
endfunction
function x=GaussSimples(a,b,n)
  x=zeros(n,1); //Ax=b :.. x=A\b
  for k=1:n-1
    for i=k+1:n
      mult=a(i,k)/a(k,k);
      a(i,k)=mult;
      for j=k+1:n
        a(i,j)=a(i,j)-mult*a(k,j); //eliminação
      end
      b(i)=b(i)-mult*b(k);
    end
  end
  x(n)=b(n)/a(n,n);
  for i=n-1:-1:1
    x(i)=b(i);
    for j=i+1:n
      x(i)=x(i)-a(i,j)*x(j); //substituição
    end
    x(i)=x(i)/a(i,i);
  end
endfunction

  //SCILAB OUTPUT
    //Exercício 9
      a=[0.0000033455 69.383 45.231;5.3400 7.3402 3.4543;32.345 3.2345 31.456];
      b=[58.222;32.554;12.563];
      n=3;
      plot(a,b) //linhas coloridas no gráfico (imagem "ex9.png")
      
  //Utilizando Eliminação de Gauss com Pivotação Parcial
    //Função: GaussPivotacao(a,b)
      x=gausspivotacao(a,b)
      plot2d(x) //linha preta no gráfico (imagem "ex9.png")

  //Utilizando Eliminação de Gauss com Pivotação Parcial
    //Função: GaussSimples(a,b)
      x=GaussSimples(a,b,n)
      plot2d(x) //linha preta no gráfico (imagem "ex9.png")
    
    
    //Comparando-os utilizando-se de plot e plot2d
    //Onde plot(a,b) irá plotar a matriz
    //e plot2d irá plotar os valores de x
    //que nesse caso entre x=gausspivotacoes e x=Gausssimples
    //tem o mesmo plot
