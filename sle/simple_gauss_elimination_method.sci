while i<=m && j<=n do
  //Find pivot in column j, starting in row i:
  maxi=i;
  for k=i+1:m
    if abs(a(k,j))>abs(a(maxi,j)) then
      maxi=k;
    end
  end
  if a(maxi,j)~=0 then
    //swap rows i and maxi, but do not change the value of i
    //Now A[i,j] will contain the old value of A[maxi,j].
    //divide each entry in row i by A[i,j]
    //Now A[i,j] will have the value 1.
    for u=i+1:m
      //subtract A[u,j] * row i from row u
      //Now A[u,j] will be 0, since A[u,j] - A[i,j] * A[u,j] = A[u,j] - 1 * A[u,j] = 0.
    end
    i=i+1;
  end
  j=j+1;
end