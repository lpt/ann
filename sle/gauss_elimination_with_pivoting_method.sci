//Sobre o algoritimo:
//Existem dúvidas quando se tem erros de arredondamento, e quando esses erros são muito pequenos e estão em função de números muito pequenos, 
//nesse algoritimo por exemplo, que trabalha a eliminação de Gauss com Pivotamento Parcial, não podemos ter 100% de certeza quando se tem coeficientes
//muito perto de 0, não da para saber se esse coeficiente é nulo mesmo, devido a ineficiência desse algorítimo, pois ele também depende da eficiência
//e complexidade do hardware, que infalívelmente, irá arredondar. E nesse arredondamento existem as perdas que acarretam em mais perdas. Para maioria
//dos casos, o código é válido, ele acha x-ésimos muito bons para fins computacionais. De qualquer forma uma solução seria um tolerância implementada
//para coeficientes no algoritimo, que interpretasse algo muito próximo de 0, como sendo nulo. E assim alertando como sistema sem solução.


function x=egpp(a,b) //e.g.p.p. sigla eliminação de gauss com pivotação parcial
  n=size(a,"r") //função size, é uma lista e retorna numero de elementos.
  a=zeros(1,n-1) //arranjando a matriz logo após saber o número de elementos
  for k=1:n-1
    mu=k
    pivo=abs(a(mu,k))
    for i=k:n
      if (abs(a(i,k))>pivo) then //if para o bubble-sort da troca de linha
        mu=i;
        pivo=abs(a(i,k)) //abs() funcao que me retorna o valor em módulo
      end                //para escolha do maior pivô da coluna.
    end
    for j=k:n
      temp=a(k,j);     //temp sigla para temporario
      a(k,j)=a(mu,j);  //troca de linha
      a(mu,j)=temp;    //troca simples usando bubble-sort
    end
    p(k)=mu
    for i=k+1:n
      a(i,k)=a(i,k)/a(k,k);
    end
    for i=k+1:n
      for j=k+1:n
        a(i,j)=a(i,j)-a(i,k)*a(k,j);
      end
    end
  end
  for k=1:n-1
    temp=b(k);
    mu=p(k);
    b(k)=b(mu);
    b(mu)=temp;
    for i=k+1:n
      b(i)=b(i)-b(k)*a(i,k);
    end
  end
  for k=n:-1:1
    t=0.
    for j=k+1:n
      t=t+a(k,j)*b(j)
    end
    b(k)=(b(k)-t)/a(k,k)
  end
  x=b;
endfunction //x=egpp(a,b)