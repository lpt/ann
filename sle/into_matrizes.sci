//Lucas Pagotto Tonussi - Análise Numérica
//15/4/2011

//3 Sistemas de Equações Lineares (12h)
//3.1 Métodos Diretos
//3.1.1 Eliminação de Gauss
//3.2 Métodos Iterativos
//3.2.1 Método de Gauss-Jacobi
//3.2.2 Método de Gauss-Seidel
//3.3 Aplicações
//3.4 Sistemas não-lineares
//3.5 Implementação
//
//4. Interpolação Numérica (12h)
//4.1 Polinômio Interpolador
//4.2 Interpolação por Lagrange
//4.3 Interpolação por Newton
//4.4 Interpolação - Diferenças Finitas
//4.5 Ajustamento de Curvas
//4.6 Ajuste Linear
//4.7 Ajuste Polinomial
//4.8 Aplicações
//4.9 Implementação


//  David Hilbert (german math&ph)

//                 1 
//  H(i,j) =  -----------
//             i + j - 1

  
//  H(i,j) = ∫¹ [x^(i+j-1) dx] "Integral de 0 à 1 de x^(i+j-1) dx"
//           ⁰    
//  Solving: ∫¹ (x^a)dx :. x^(a+1) |¹               :..   x^(i+j) |¹  :...    1        (i+j)^-1 
//           ⁰             ------- |  (i+j-1) is a        ------- |         -----  or  ---------
//                           a+1   ⁰                        i+j   ⁰          i+j           1

//  i.e: 5x5 Matrix of Hilbert
//  1/1 1/2 1/3 1/4 1/5
//  1/2 1/3 1/4 1/5 1/6
//  1/3 1/4 1/5 1/6 1/7
//  1/4 1/5 1/6 1/7 1/8
//  1/5 1/6 1/7 1/8 1/9


function h=hilb(n) //call hilbert function
  h=zeros(n,n) //consider of h receve a matrix of zeros n x n = n²
  for i=1:n; //here, i (line) goes of 1 to n
    for j=i:n; //and here, j (colum) goes of i to n
      h(i,j)=1/(i+j-1) //h in the position (i,j), of the interactions above, goes making the operation (i plus j minus 1)^-1
    end
  end
endfunction
