function I = inverte (A)
  function i=maiorpivo(a,j)
    [nlin,ncol]=size(a);
    i=j;
    for k=j:nlin
      if abs(a(i,j))<abs(a(k,j))
        i=k;
      end
    end
  endfunction
  function x=trocalinhas(a,i,j)
    l=a(i,:);
    a(i,:)=a(j,:);
    a(j,:)=l;
    x=a;
  endfunction
  [nlin, ncol] = size(A);
  n = nlin;
  I = eye (n, n);
  for i = 1:n do
    m = maior_pivo (A, i);
    A = troca_linhas (A, i, m);
    I = troca_linhas (I, i, m);
    for j = (i + 1):n do
      l = A(j,i) / A(i, i);
      A(j,:) = A(j,:) - l * A(i,:);
      I(j,:) = I(j,:) - l * I(i,:);
    end
  end
    for i = n:-1:1 do
    for j = (i - 1):-1:1 do
      l = A(j,i) / A(i, i);
      A(j,:) = A(j,:) - l * A(i,:);
      I(j,:) = I(j,:) - l * I(i,:);
    end
  end
  for i = 1: n do
    I(i,:) = I(i,:) / A(i,i);
    A(i,i) = 1;
  end
endfunction


function [n,p]=seidel(a)
  [nl,nc]=size(a);
  n=nl;
  p=zeros(n,n);
  for i=1:n
    for j=(i+1):n
      p(i,j)=-a(i,j);
      a(i,j)=0;
    end
  end
  n=a;
endfunction


function [n,p]=decomposicao(a)
  [nl,nc]=size(a);
  n=nl;
  p=zeros(n,n);
  for i=1:n
    for j=i+1:n
      p(i,j)=-a(i,j);
      a(i,j)= 0;
    end
  end
  n=a;
endfunction

function n=norma(a)
  [nl,nc]=size(a);
  n=0;
  for i=1:nl
    somatorio=0;
    for j=1:nc
      somatorio=somatorio+abs(a(i,j));
    end
    if somatorio>n then
      n=somatorio;
    end
  end
endfunction
