#include <stdio.h>
#include <math.h>

int  Lower_Triangular_Solve(double *L, double B[], double x[], int n);
void Unit_Upper_Triangular_Solve(double *U, double B[], double x[], int n);

main()
{

   int Crout_LU_Decomposition(double *A, int n)
   {
      int row, i, j, k, p;
      double *p_k, *p_row, *p_col;
      for (k = 0, p_k = A; k < n; p_k += n, k++)
      {
         for (i = k, p_row = p_k; i < n; p_row += n, i++)
         {
            for (p = 0, p_col = A; p < k; p_col += n, p++)
            {
               *(p_row + k) -= *(p_row + p) * *(p_col + k);
            }
         }  
         if (*(p_k + k)==0.0)
         {
            return -1;
         }
         for (j = k+1; j < n; j++)
         {
            for (p = 0, p_col = A; p < k; p_col += n,  p++)
            {
               *(p_k + j) -= *(p_k + p) * *(p_col + j);
            }
            *(p_k + j) /= *(p_k + k);
         }
      }
      return 0;
   }
   int Crout_LU_Solve(double *LU, double B[], double x[], int n)
   {
      if ( Lower_Triangular_Solve(LU, B, x, n) < 0 )
      {
         return -1;
      }
      Unit_Upper_Triangular_Solve(LU, x, x, n);
      return 0;
   }
}
