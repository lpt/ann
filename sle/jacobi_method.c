/*
Critério de Convergência para o método de Jacobi.
Teorema: Seja o sistema linear

AX=3

e seja, 

⍺k =  1   / n               \
    ----- |∑          |akj| |
    |akk| \(j!=k E j=1)     /


Se ⍺ = max        ⍺k<1,  então o método de Jacobi gera uma sequência {x^(k)}
        {i<=k<=n}        convergente ara solução do sistema dado, independente 
                         da escolha da aproximação inicial x⁰

  /10 2  1\
A=|1  5  1|
  \2  3 10/

⍺1 .: (1/|⍺11|)*(|⍺12|+|⍺13|) .: (1/10)*(2+1) .: 3/10
⍺2 .: (1/|⍺22|)*(|⍺21|+|⍺23|) .: (1/5) *(1+1) .: 2/5
⍺3 .: (1/|⍺33|)*(|⍺31|+|⍺32|) .: (1/10)*(2+3) .: 1/2

  ⍺ = max        ⍺k<⍺3  .: 1/2<1
        {i<=k<=3}
  Processo de Jacobi converge.
  Nota: Este Teorema da uma condição suficiente, mas 
  não necessário para convergência do metodo de Jacobi.


Exercício: Mostre que o sistema seguinte converge, 
apesar de não satisfazer o critério de convergêcia.

{ x[1] + 3x[2] +  x[3] =-2
{5x[1] + 2x[2] + 2x[3] =+3
{        6x[2] + 8x[3] =-6
*trocar linhas por colunas se necessário.*

Método de Gauss-Seidel
AX=y

/11 12 13 ... 1n\
|21 22 23 ... 2n|
|.        .    .|
|.         .   .|
|.          .  .|
\n1 n2 n3 ... nn/

/ 0  0  0 ...     0\
|21  0  0 ...     0|
|31  32 0          |
|.                .|
|.                .|
|.                .|
\n1 n2 n3 an(n-1) 0/

/0 12  1n\
|0  0  2n|
|        |
\0      0/

/11 0  0  0  0\
|0 22  0  0  0|
|0  0 33  0  0|
|0  0  0 44  0|
\0  0  0  0 nn/


AX(L+V+D)*x=y
DX=-LX-VX+y
X=D⁻¹Y-D⁻¹LX-D⁻1VX

Processo de iteração
X^⁽k+1⁾=D⁻¹Y-D⁻¹LX⁽k⁾-D⁻¹VX⁽k⁾

Gauss-Seidel
X⁽⁾

Exemplo

x[1] +  x[2]= 3
x[1] - 3x[2]=-3


Jacobi
x[1]⁽k+1⁾ = 3 - x[2]⁽k⁾
x[2]⁽k+1⁾ = 1 + (1/3)x[1]⁽k⁾

Gaus-Seidel:
x[1]⁽k+1⁾ = 3 - x[2]⁽k⁾
x[2]⁽k+1⁾ = 1 + (1/3)x[1]⁽k⁾


Exercício, Considere o sistema acima, ilestre geometricamente o processo iterativo, com x[1]⁽⁰⁾
x[2]⁽⁰⁾=0, nos métodos de Jacobi e Gauss-Seidel.

Exercício

5x[1] +  x[2] +  x[3]=5
3x[1] + 4x[2] +  x[3]=6
3x[1] + 3x[2] + 6x[3]=0

Processo iterativo de G-S:
x[1]⁽k+1⁾ = (5/5) -  x[2]⁽k⁾/5   -  x[3]⁽k⁾/5
x[2]⁽k+1⁾ = (6/4) - 3x[1]⁽k+1⁾/4 -  x[3]⁽k⁾/4
x[3]⁽k+1⁾ = (0)   - 3x[2]⁽k⁾/6   - 3x[2]⁽k+1⁾/6

Critério de Parada.
Erro: Distância entre X⁽k+1⁾ X⁽k⁾

d⁽k⁾= max           |xi⁽k⁾ - xi⁽k-1⁾|
        {i<=i<=n}
    =|x⁽k⁾ - x⁽k-1⁾|inf


Erro relativo: d⁽k⁾/x⁽k⁾


Faça iterações até que o erro relativo E seja menor que 10⁻4
Calcule o número de iterações necessárias para que este erro seja atingedo, usando o método de Jacobi.
*/
