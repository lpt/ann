function [n,p]=seidel(a)
  [nl,nc]=size(a);
  n=nl;
  p=zeros (n,n);
  for i=1:n
    for j=i+1:n
      p(i,j)=-a(i,j);
      a(i,j)= 0;
    end
  end
  n=a;
endfunction

