function [z]=zero_newraph(f,x0,Nmax,norma); // M�todo Newton-Raphson

//dados de entrada

// f        -> fun��o cujo zero ser� determinado.
// x0       -> aproxima��o incial.
// Nmax     -> limite superior para o n�mero de itera��es.
// norma    -> norma vetorial a ser utilizada.
// tol_err  -> erro absoluto admiss�vel na solu��o.

//inicialiaza��o das vari�veis

x1=x0+derivative(f,x0)\(-f(x0));
old_abs_diff=norm(x0-x1,norma);
x0=x1;
segue=%T;
contador=1;

//la�o principal

while segue
  contador=contador+1;
  x1=x0+derivative(f,x0)\(-f(x0));
  abs_diff=norm(x0-x1,norma);
  // descomente as duas linhas abaixo se quiseres a sequ�ncia de itera��es no console
  //mprintf('Itera��o %d\n',contador);
  //mprintf('aproxima��o: %.17e\n',contador,x1);
  if (abs_diff<=%eps*norm(x1,norma) & abs_diff>=old_abs_diff)| contador>=Nmax then
     segue=%F;   // teste para o prosseguimento do la�o principal.
  end;
  x0=x1;
  old_abs_diff=abs_diff;
end; 

// Sa�da de dados

if contador>=Nmax then 
  warning(msprintf('\n A exatid�o n�o foi obtida em %d iteracoes',Nmax));
else
  warning(msprintf('Foram necess�rias %d itera��es \n\n',contador));
end;
z=x1;

endfunction
