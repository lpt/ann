//The above algorithm gives the most straightforward explanation towards the conjugate gradient method.
//The algorithm is detailed below for solving Ax=b where A is a real, symmetric,positive-definite matrix.The input vector x0 can be an approximate initial solution or 0

function x=conjgrad(A,b,x)
    r=b-A*x
    p=r
    r=sold=r^*r
    for k := 1 to size(A,1)
        alpha=rsold/(p^*Ap)
        x=x+alpha*p
        r=r-alpha*Ap
        rsnew=r'*r
        if sqrt(rsnew)<1e-10 then
            break
        end
        p=r+rsnew/rsold*p
        rsold=rsnew
    end
endfunction

A = [4,1;1,3]
b = [1;2]
x = [2;1]
x=conjgrad(A,b,x)