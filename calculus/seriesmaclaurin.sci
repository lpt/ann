function y=maclaurin(theta)
    t=1
    for k := 1 to 5
      y=((theta^(2*k+1))*(!t)^k))/(fact(2k+1))
    end
endfunction

theta=3;
y=maclaurin(theta)
